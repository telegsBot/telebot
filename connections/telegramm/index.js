const TelegramBot = require('node-telegram-bot-api');

class Telegramm {
  constructor() {
    this.connection = Telegramm.connectToTelegramm();

    return this.connection;
  }

  static connectToTelegramm() {
    // return new TelegramBot(process.env.TELEGRAMM_CONNECT, { polling: true });
    return new TelegramBot(process.env.TELEGRAMM_CONNECT, { polling: true });
  }
}

module.exports = Telegramm;