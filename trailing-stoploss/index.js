const { TelegramClient } = require('messaging-api-telegram');

const clientTeleg = TelegramClient.connect(process.env.TELEGRAMM_CONNECT);

class Trailingstoploss {
  constructor(connection, userId = '') {
    this.redissubs = connection.redissubs;
    this.mysql = connection.mysqldb;

    this.userId = userId;

    this.listTrialUsers = [];
    this.listSubUsers = [];
    // this.telegrammrobot = connection.telegrammrobot;

    return this;
  }

  formListOfActiveTrailingUsers() {
    return new Promise((resolve, reject) => {
      const sql = 'SELECT ( DATE_ADD(NOW(), INTERVAL 3 HOUR) < DATE_ADD(DATE, INTERVAL 5 MINUTE) ) AS diff FROM `god_db1`.`trailing_stoploss`;';
      this.mysql.query(sql, (error, results) => {
        if (error) {
          // console.log(error.sqlMessage);
          reject(error.sqlMessage);
        }

        // console.log(results[0].diff);
        resolve(results[0].diff);
      });
    });
  }

  async chooseShowingBetweenTrialAndSub1() {
    let submenuTrailing = 'trial-trailing';
    const resultDiffTrial = await this.isItTrueBetweenNowAndDateTrialOrSub('DATE_TRIAL', '7', 0);
    const resultDiffSub = await this.isItTrueBetweenNowAndDateTrialOrSub('DATE_SUB', '30', 1);
    const resultStatusPayment = await this.getCurrentStatusPayment();

    switch (resultDiffTrial) {
      case 0: // Да, истекла
        // Показываем выбор платной подписки
        if (resultDiffSub === 0 || resultDiffSub === null) {
          if (resultStatusPayment === 0 || resultStatusPayment === null) {
            // submenuTrailing = 'subscription-trailing';
            if (resultStatusPayment === 0) submenuTrailing = 3;
            if (resultStatusPayment === null) submenuTrailing = 1;
          } else {
            // submenuTrailing = 'trailing-trial-no';
            submenuTrailing = 2;
          }
        }
        if (resultDiffSub === 1) {
          // submenuTrailing = 'trailing-trial-no';
          submenuTrailing = 2;
        }
        break;
      case 1: // Нет, фри версия еще не истекла
        // submenuTrailing = 'trailing-trial-no';
        submenuTrailing = 2;
        break;
      default:
        break;
    }
    return (submenuTrailing);
  }

  async chooseShowingBetweenTrialAndSub() {
    let submenuTrailing = 'trial-trailing';
    const resultDiffTrial = await this.isItTrueBetweenNowAndDateTrialOrSub('DATE_TRIAL', '7');
    const resultDiffSub = await this.isItTrueBetweenNowAndDateTrialOrSub('DATE_SUB', '30');

    const resultStatusPayment = await this.getCurrentStatusPayment();

    console.log('resultDiffTrial:', resultDiffTrial, 'resultDiffSub:', resultDiffSub, 'resultStatusPayment:', resultStatusPayment);
    switch (resultDiffTrial[0]) {
      case null: // Юзер вообще еще ничего не делал
        submenuTrailing = 0;
        break;
      case 0: // Да, истекла
        // Показываем выбор платной подписки
        switch (resultDiffSub[1]) {
          case 0:
            // show paid
            if (resultStatusPayment === null) submenuTrailing = 1;
            // wait payment
            if (resultStatusPayment === 0) submenuTrailing = 2;
            // show paid NEW
            if (resultStatusPayment === 1) submenuTrailing = 5;
            break;
          case 1:
            // ALL OK. SUB LIVES
            submenuTrailing = 3;
            break;
          default:
            break;
        }
        break;
      case 1: // Нет, фри версия еще не истекла
        // submenuTrailing = 'trailing-trial-no';
        submenuTrailing = 4;
        break;
      default:
        break;
    }
    return (submenuTrailing);
  }

  isItTrueBetweenNowAndDateTrialOrSub(field, interval) {
    return new Promise((resolve, reject) => {
      const sql = `SELECT active, DATE_ADD(NOW(), INTERVAL 3 HOUR) < DATE_ADD(${field}, INTERVAL ${interval} DAY) AS diff FROM \`god_db1\`.\`trailing_stoploss\` WHERE (userid = ${this.userId}) LIMIT 1;`;
      this.mysql.query(sql, (error, results) => {
        if (error) {
          reject(error.sqlMessage);
        }

        if (results.length !== 0) {
          resolve([results[0].diff, results[0].active]);
        } else {
          resolve([null, 0]);
        }
      });
    });
  }

  checkExistenceUser() {
    return new Promise((resolve, reject) => {
      this.mysql.query('SELECT * FROM `god_db1`.`trailing_stoploss` WHERE userid = ?', [this.userId], (error, results) => {
        if (error) {
          console.log(error.sqlMessage);
          reject(error.sqlMessage);
        }

        resolve(results);
      });
    });
  }

  getCurrentStatusPayment() {
    return new Promise((resolve, reject) => {
      this.mysql.query('SELECT `status` FROM `god_db1`.`users_payments` WHERE (userid = ? AND type_sub = ?) ORDER BY id DESC LIMIT 1', [this.userId, 3], (error, results) => {
        if (error) {
          console.log(error.sqlMessage);
          reject(error.sqlMessage);
        }

        if (results.length !== 0) {
          resolve(results[0].status);
        } else {
          resolve(null);
        }
      });
    });
  }

  addNewUser() {
    return new Promise((resolve, reject) => {
      const sql = 'INSERT IGNORE INTO `god_db1`.`trailing_stoploss` SET ?';
      const activeTrialOrSub = false;
      const datas = {
        userid: this.userId,
        date_trial: new Date(),
        active: activeTrialOrSub,
      };

      this.mysql.query(sql, datas, (error, results) => {
        if (error) {
          // console.log(error.sqlMessage);
          reject(error);
        }

        resolve(results.insertId);
      });
    });
  }

  addNewPayment() {
    return new Promise((resolve, reject) => {
      const sql = 'INSERT IGNORE INTO `god_db1`.`users_payments` SET ?';
      const activeTrialOrSub = false;
      const datas = {
        userid: this.userId,
        wallet: '',
        invoice_our_id: `${this.userId}-3`,
        invoce_date: '',
        type_sub: 3,
        payment_code: '',
        invoice_id_other: '',
        price: 100000, // 0.01 BTC = 1000000
        status: activeTrialOrSub,
      };

      this.mysql.query(sql, datas, (error, results) => {
        if (error) {
          // console.log(error.sqlMessage);
          reject(error);
        }

        resolve(results.insertId);
      });
    });
  }

  monitoringStoploss() {
    this.redissubs.redis1.subscribe('buy', 'sell', 'trailing', () => {
      this.redissubs.redis1.on('message', (channel, message) => {
        console.log(channel);
        if (channel === 'trailing') {
          this.sendTrailingViaTelegramm(message);
        }

        if (channel === 'buy') {
          this.sendBuyViaTelegramm(message);
        }

        if (channel === 'sell') {
          this.sendSellViaTelegramm(message);
        }
      });
    });

    this.redissubs.redis2.subscribe('buy', 'sell', 'trailing', () => {
      this.redissubs.redis2.on('message', (channel, message) => {
        console.log(channel);
        if (channel === 'trailing') {
          this.sendTrailingViaTelegramm(message);
        }

        if (channel === 'buy') {
          this.sendBuyViaTelegramm(message);
        }

        if (channel === 'sell') {
          this.sendSellViaTelegramm(message);
        }
      });
    });

    this.redissubs.redis3.subscribe('buy', 'sell', 'trailing', () => {
      this.redissubs.redis3.on('message', (channel, message) => {
        console.log(channel);
        if (channel === 'trailing') {
          this.sendTrailingViaTelegramm(message);
        }

        if (channel === 'buy') {
          this.sendBuyViaTelegramm(message);
        }

        if (channel === 'sell') {
          this.sendSellViaTelegramm(message);
        }
      });
    });

    this.redissubs.redis4.subscribe('buy', 'sell', 'trailing', () => {
      this.redissubs.redis4.on('message', (channel, message) => {
        console.log(channel);
        if (channel === 'trailing') {
          this.sendTrailingViaTelegramm(message);
        }

        if (channel === 'buy') {
          this.sendBuyViaTelegramm(message);
        }

        if (channel === 'sell') {
          this.sendSellViaTelegramm(message);
        }
      });
    });

    this.redissubs.redis5.subscribe('buy', 'sell', 'trailing', () => {
      this.redissubs.redis5.on('message', (channel, message) => {
        console.log(channel);
        if (channel === 'trailing') {
          this.sendTrailingViaTelegramm(message);
        }

        if (channel === 'buy') {
          this.sendBuyViaTelegramm(message);
        }

        if (channel === 'sell') {
          this.sendSellViaTelegramm(message);
        }
      });
    });
  }

  monitorTrialUsers(callback) {
    this.listTrialUsers = [];
    const sql = 'SELECT * FROM `god_db1`.`trailing_stoploss` WHERE DATE_ADD(NOW(), INTERVAL 3 HOUR) <= DATE_ADD(DATE_TRIAL, INTERVAL 7 DAY);';
    this.mysql.query(sql, (error, results) => {
      if (error) {
        console.log(error.sqlMessage);
        return callback(error.sqlMessage);
      }

      Object.keys(results).forEach((key) => {
        this.listTrialUsers.push(results[key].userid);
      });

      return callback(null, results);
    });
  }

  disableUserStoplossExpired() {
    const sql = 'UPDATE `god_db1`.`trailing_stoploss` SET `active` = 0 WHERE ( DATE_ADD(NOW(), INTERVAL 3 HOUR) > DATE_ADD(DATE_SUB, INTERVAL 30 DAY) AND `active` = 1);';
    this.mysql.query(sql, (error, results) => {
      if (error) {
        console.log(error.sqlMessage);
        return (error.sqlMessage);
      }

      console.log('UPDATE trailing_stoploss', results);
      return results;
    });
  }

  getHowLongSubsLast(period) {
    return new Promise((resolve, reject) => {
      let field = 'DATE_TRIAL';
      if (period === 7) field = 'DATE_TRIAL';
      if (period === 30) field = 'DATE_SUB';
      const sql = `SELECT DATE_FORMAT( (DATE_ADD(${field}, INTERVAL ${period} DAY)) , '%d %b %Y %H:%i') as d FROM \`god_db1\`.\`trailing_stoploss\` WHERE (userid = ${this.userId}) LIMIT 1;`;
      this.mysql.query(sql, (error, results) => {
        if (error) {
          console.log(error.sqlMessage);
          reject(error.sqlMessage);
        }

        if (results.length !== 0) {
          resolve(results[0].d);
        } else {
          resolve(null);
        }
      });
    });
  }

  getHowLongSubsLastApiTrading(period) {
    return new Promise((resolve, reject) => {
      // %d %b %Y %H:%i
      const sql = `SELECT DATE_FORMAT( (DATE_ADD(DATE_SUB, INTERVAL ${period} DAY)) , '%d %b %Y') as d FROM \`god_db1\`.\`api_trading\` WHERE (userid = ${this.userId}) LIMIT 1;`;
      this.mysql.query(sql, (error, results) => {
        if (error) {
          console.log(error.sqlMessage);
          reject(error.sqlMessage);
        }

        if (results.length !== 0) {
          resolve(results[0].d);
        } else {
          resolve(null);
        }
      });
    });
  }

  getKeysAndSizeOrder() {
    return new Promise((resolve, reject) => {
      // %d %b %Y %H:%i
      const sql = `SELECT * FROM \`god_db1\`.\`api_trading\` WHERE (userid = ${this.userId}) LIMIT 1;`;
      this.mysql.query(sql, (error, results) => {
        if (error) {
          console.log(error.sqlMessage);
          reject(error.sqlMessage);
        }

        if (results.length !== 0) {
          const map = new Map();
          map.set('apikey', results[0].apikey);
          map.set('secretkey', results[0].secretkey);
          map.set('order', results[0].buy_order);
          resolve(map);
        } else {
          const map = new Map();
          map.set('apikey', '');
          map.set('secretkey', '');
          map.set('order', '');
          resolve(map);
        }
      });
    });
  }

  monitorSubUsers(callback) {
    this.listSubUsers = [];
    const sql = 'SELECT * FROM `god_db1`.`trailing_stoploss` WHERE DATE_ADD(NOW(), INTERVAL 3 HOUR) < DATE_ADD(DATE_SUB, INTERVAL 30 DAY);';
    this.mysql.query(sql, (error, results) => {
      if (error) {
        console.log(error.sqlMessage);
        return callback(error.sqlMessage);
      }

      Object.keys(results).forEach((key) => {
        this.listSubUsers.push(results[key].userid);
      });

      return callback(null, results);
    });
  }

  sendTrailingViaTelegramm(message) {
    const trailing = JSON.parse(message);
    const symbol = trailing.Symbol.replace('BTC', '-BTC');
    const stoploss = trailing.Fixprice.toFixed(8);

    let msgTrial = `🔔 ${symbol}`;
    msgTrial += `\n💸 MOVE STOP-LOSS TO: ${stoploss}`;

    let msgSub = `🔔 ${symbol}`;
    msgSub += `\n💸 MOVE STOP-LOSS TO: ${stoploss}`;


    if (this.listTrialUsers.length !== 0) {
      this.listTrialUsers.forEach((dataset) => {
        clientTeleg.sendMessage(dataset, msgTrial, {
          disable_web_page_preview: true,
          disable_notification: true,
        });
      });
    }

    if (this.listSubUsers.length !== 0) {
      this.listSubUsers.forEach((dataset) => {
        clientTeleg.sendMessage(dataset, msgSub, {
          disable_web_page_preview: true,
          disable_notification: true,
        });
      });
    }
  }

  sendBuyViaTelegramm(message) {
    const buying = JSON.parse(message);
    const symbol = buying.Symbol.replace('BTC', '-BTC');
    const price = buying.Buyprice;
    const stoploss = buying.Stoploss.toFixed(8);

    let msgBuyTrial = `🔔 ${symbol}`;
    msgBuyTrial += `\n📗 BUY PRICE: ${price}`;
    msgBuyTrial += `\n⛔️ STOPLOSS: ${stoploss}`;

    let msgBuySub = `🔔 ${symbol}`;
    msgBuySub += `\n📗 BUY PRICE: ${price}`;
    msgBuySub += `\n⛔️ STOPLOSS: ${stoploss}`;

    if (this.listTrialUsers.length !== 0) {
      this.listTrialUsers.forEach((dataset) => {
        clientTeleg.sendMessage(dataset, msgBuyTrial, {
          disable_web_page_preview: true,
          disable_notification: true,
        });
      });
    }

    if (this.listSubUsers.length !== 0) {
      this.listSubUsers.forEach((dataset) => {
        clientTeleg.sendMessage(dataset, msgBuySub, {
          disable_web_page_preview: true,
          disable_notification: true,
        });
      });
    }
  }

  sendSellViaTelegramm(message) {
    const selling = JSON.parse(message);
    const symbol = selling.Symbol.replace('BTC', '-BTC');
    const sellprice = selling.Fixprice;

    let msgSellTrial = `🔔 ${symbol}`;
    msgSellTrial += `\n📕 SELL PRICE: ${sellprice}`;

    let msgSellSub = `🔔 ${symbol}`;
    msgSellSub += `\n📕 SELL PRICE: ${sellprice}`;

    if (this.listTrialUsers.length !== 0) {
      this.listTrialUsers.forEach((dataset) => {
        clientTeleg.sendMessage(dataset, msgSellTrial, {
          disable_web_page_preview: true,
          disable_notification: true,
        });
      });
    }

    if (this.listSubUsers.length !== 0) {
      this.listSubUsers.forEach((dataset) => {
        clientTeleg.sendMessage(dataset, msgSellSub, {
          disable_web_page_preview: true,
          disable_notification: true,
        });
      });
    }
  }

  static isEmptyObj(foo) {
    let result = true;
    Object.keys(foo).forEach((key) => {
      if (Object.prototype.hasOwnProperty.call(foo, key)) {
        result = false;
      }
    });
    return result;
  }
}

module.exports = Trailingstoploss;