// const Dbredis = require('./redis');

// const redis = new Dbredis();

const Dbpostgre = require('./postgre');

const postgre = new Dbpostgre();
postgre.connect();

const Dbmysql = require('./mysqldb');

const mysqldb = new Dbmysql();

const Dbredissub = require('./redis');

const redissubs = new Dbredissub();

const Telegramm = require('./telegramm');

const telegrammrobot = new Telegramm();

class Connection {
  constructor() {
    // this.redis = redis;
    this.postgre = postgre;
    this.mysqldb = mysqldb;
    this.telegrammrobot = telegrammrobot;
    this.redissubs = redissubs;

    return this;
  }
}

module.exports = Connection;