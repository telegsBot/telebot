// eslint:airbnb-base
require('dotenv').config();

const Connections = require('./connections');

const connections = new Connections();
// let webhook = connections.telegrammrobot.setWebHook(`https://earncrypto.site/bot${process.env.TELEGRAMM_CONNECT}`);

const Menu = require('./menu');

const menu = new Menu();

const Telegrammservice = require('./telegramm-service');

const Trailingstoploss = require('./trailing-stoploss');

const trailingstoplossMonitoring = new Trailingstoploss(connections);
// trailingstoplossMonitoring.monitoringStoploss();

const Apitrading = require('./api-trading');

const apitrading = new Apitrading(connections);

const Newuser = require('./new-user');

const newuser = new Newuser(connections);

const lastMsgId = new Map();
const map = new Map();
const apiselect = new Map();
const wallet = new Map();

apiselect.set('api', 'no');
const commands = ['/start', '/help', '/menu', 'api-key', 'secret-key', 'order-size'];

trailingstoplossMonitoring.monitorTrialUsers((error, result) => {
  if (error) {
    console.log(error);
  } else {
    console.log('trial', trailingstoplossMonitoring.listTrialUsers);
  }
});

trailingstoplossMonitoring.monitorSubUsers((error, result) => {
  if (error) {
    console.log(error);
  } else {
    console.log('sub', trailingstoplossMonitoring.listSubUsers);
  }
});

apitrading.monitorApiTraidingSubUsers((error, result) => {
  if (error) {
    console.log(error);
  } else {
    console.log('api', apitrading.listApiSubUsers);
    apitrading.monitoringBuySell();
  }
});


apitrading.monitorApiTraidingSubExpiredTrialUsers((error) => {
  if (error) {
    console.log(error);
  } else {
    console.log('sell operation for trial api users', apitrading.listApiSubTrialExpiredUsers);
  }
});

connections.telegrammrobot.onText(/\/(start)|(menu)/, async (msg) => {
  if (!msg.from.is_bot) {
    const foo = {
      id: msg.from.id,
      firstName: msg.from.first_name,
      isBot: msg.from.is_bot,
      languageCode: msg.from.language_code,
    };
    newuser.addNewUser(foo);

    const telegrammservice = new Telegrammservice(msg.chat.id, connections);
    await telegrammservice.delAllFieldsRedis(msg.from.id);
    await telegrammservice.setOneFieldRedis(msg.from.id, 'apidata', 'no');

    if (!map.has(msg.from.id)) {
      map.set(msg.from.id, true);
    } else {
      connections.telegrammrobot.deleteMessage(msg.chat.id, lastMsgId.get(msg.chat.id));
    }

    const trailingstoploss = new Trailingstoploss(connections, msg.from.id);

    let stateApi = 0;
    let stateTrailing = 0;
    // // ====================================
    // // Существует ли данный юзер в таблице подписок по трейлингу
    // const resultExistence = await trailingstoploss.checkExistenceUser();
    // console.log('resultExistence:', resultExistence);
    // if (Trailingstoploss.isEmptyObj(resultExistence)) { // Нет
    //   // Показываем выбор фри и добавляем его в таблицу
    //   // await trailingstoploss.addNewUser(msg.from.id);
    //   stateTrailing = 0;
    // } else { // Да. Проверка подписок. Что по итогу выводить в меню
    //   // submenuTrailing = await trailingstoploss.chooseShowingBetweenTrialAndSub();
    //   stateTrailing = await trailingstoploss.chooseShowingBetweenTrialAndSub();
    // }
    // // ====================================

    // // ====================================
    // // Существует ли данный юзер в таблице подписок по API
    // const resultExistenceApi = await apitrading.checkExistenceUser(msg.from.id);
    // // console.log(resultExistenceApi);
    // if (Apitrading.isEmptyObj(resultExistenceApi)) { // Нет
    //   // Показываем меню с вводом данных
    //   stateApi = 0;
    // } else { // Да. Показыем пустое меню
    //   stateApi = 1;
    // }
    // // ====================================

    // ====================================
    // Существует ли данный юзер в таблице подписок по трейлингу
    const resultExistence = await trailingstoploss.checkExistenceUser();
    if (Trailingstoploss.isEmptyObj(resultExistence)) { // Нет
      // Показываем выбор фри и добавляем его в таблицу
      // await trailingstoploss.addNewUser(msg.from.id);
      stateTrailing = 0;
    } else { // Да. Проверка подписок. Что по итогу выводить в меню
      // submenuTrailing = await trailingstoploss.chooseShowingBetweenTrialAndSub();
      stateTrailing = await trailingstoploss.chooseShowingBetweenTrialAndSub();
    }
    // ====================================

    // ====================================
    // Существует ли данный юзер в таблице подписок по API
    const resultExistenceApi = await apitrading.checkExistenceUser(msg.from.id);
    if (Apitrading.isEmptyObj(resultExistenceApi)) { // Нет
      // Показываем меню с вводом данных
      const r = await apitrading.checkLastPaymentUser(msg.from.id);
      const r2 = await apitrading.checkLastApiTrialUserStatus(msg.from.id);
      if (r === null) stateApi = 0; // Его нет действительно
      if (r === 0) stateApi = 1; // Он есть. Сделал запрос на кошель. Ждём оплаты
      if (r === 1) stateApi = 2; // Он есть. Но подписка истекла и он не сделал запрос
      // stateApi = 0;

      // if (msg.from.id === 402865625 || msg.from.id === 159856577) {
      console.log(msg.from.id);
      // Это триал версия
      if (r === null && r2 === null) stateApi = -4;

      // Триал активирован
      if (r === null && r2 === 0) stateApi = -3;

      // Триал закончен, но есть открытые сделки
      // или еще не проверен юзер на наличие таковых
      if (r === null && r2 === 1) stateApi = -2;

      // Триал закончен и проверка
      // на открытые сделки не производится
      // !!! НЕ заюыть, поменять значение поля
      // при вызове info_new: чтобы в таблице
      // менялось значение active_trial на `2`
      if (r === null && r2 === 2) stateApi = -1;
      // }
    } else { // Да. Показыем пустое меню
      stateApi = 3; // Пользователь с активной подпиской
    }
    // ====================================

    const keysAndOrderResults = await trailingstoploss.getKeysAndSizeOrder();
    let apikeyUser = keysAndOrderResults.get('apikey') === '' ? 'N/A' : keysAndOrderResults.get('apikey');
    const apiSecretUser = keysAndOrderResults.get('secretkey') === '' ? 'N/A' : '****';
    let buyOrderUser = keysAndOrderResults.get('order');
    let buyOrderUserTF = true;
    if (keysAndOrderResults.get('order') === parseFloat('0.002')) {
      buyOrderUser = '0.002 BTC (value by defaults)';
      buyOrderUserTF = false;
    }
    if (keysAndOrderResults.get('order') === '') {
      buyOrderUser = 'N/A';
      buyOrderUserTF = false;
    }
    if (buyOrderUserTF) buyOrderUser += ' BTC';
    // const buyOrderUser = keysAndOrderResults.get('order') === parseFloat('0.002') ? '0.002 BTC (default)' : keysAndOrderResults.get('order');
    let apikeyUser1 = '';
    let apikeyUser2 = '';
    if (apikeyUser !== 'N/A') {
      apikeyUser1 = apikeyUser.substring(0, 4);
      apikeyUser2 = apikeyUser.substring(apikeyUser.length - 4);
      apikeyUser = `${apikeyUser1}****${apikeyUser2}`;
    }
    let robotStatus = '';
    let robotStatusinactive = '';
    let robotTrialStatus = '';
    let robotTrialStatusStart = '';
    if (apikeyUser !== '' && apikeyUser !== 'N/A'
        && apiSecretUser !== '' && apiSecretUser !== 'N/A') {
      robotStatus = 'robot is working';
      robotStatusinactive = 'Robot doesn\'t work. You have to pay subscription.';
      robotTrialStatus = 'TRIAL robot is working';
      robotTrialStatusStart = 'Activate your TRIAL';
    } else {
      robotStatus = 'API Key or Secret Key is empty';
      robotStatusinactive = 'API Key or Secret Key is empty';
      robotTrialStatus = 'API Key or Secret Key is empty';
      robotTrialStatusStart = 'API Key or Secret Key is empty';
    }

    let text = '🎯 <b>Binance API Trading Tool</b>\n\n';
    text += 'This tool fully automates the trading by signals, ';
    text += 'sending commands: to buy and to sell directly to your <a href="https://www.binance.com/?ref=20390268">Binance.com</a> ';
    text += 'account via API interface. 0.05 BTC for lifetime.\n\n';
    text += 'In this section you can check the status and renew your subscription. ';
    text += 'Also you can set up your API Keys and Order Size.\n\n';
    text += '<b>ATTENTION!!</b> <i>Login your Binance.com account then you have to create and to set up ';
    text += 'API KEY checkboxes:</i> <b>INFO</b> <i>and</i> <b>TRADING</b>.\n\n';

    // console.log(apikeyUser, apiSecretUser, buyOrderUser);
    console.log(stateApi);
    if (stateApi === -4) { // New user
      // connections.telegrammrobot.deleteMessage(chatId, msgId);
      text += `API Key: ${apikeyUser}\n`;
      text += `Secret Key: ${apiSecretUser}\n`;
      text += `Order size: ${buyOrderUser}\n`;
      text += `Status: ${robotTrialStatusStart}\n\n`;
      const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('payment-enter-api-datas-t'));
      lastMsgId.set(payload.chat.id, payload.message_id);
    }

    if (stateApi === -3) { // New user
      // connections.telegrammrobot.deleteMessage(chatId, msgId);
      text += `API Key: ${apikeyUser}\n`;
      text += `Secret Key: ${apiSecretUser}\n`;
      text += `Order size: ${buyOrderUser}\n`;
      text += `Status: ${robotTrialStatus}\n\n`;
      const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('enter-api-datas'));
      lastMsgId.set(payload.chat.id, payload.message_id);
      await telegrammservice.setOneFieldRedis(payload.chat.id, 'payload', payload.message_id);
    }

    if (stateApi === -2) { // New user
      // connections.telegrammrobot.deleteMessage(chatId, msgId);
      text += `API Key: ${apikeyUser}\n`;
      text += `Secret Key: ${apiSecretUser}\n`;
      text += `Order size: ${buyOrderUser}\n`;
      text += `Status: ${robotStatusinactive}\n\n`;
      const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('payment-enter-api-datas'));
      lastMsgId.set(payload.chat.id, payload.message_id);
      await telegrammservice.setOneFieldRedis(payload.chat.id, 'payload', payload.message_id);
    }

    if (stateApi === -1) { // New user
      // connections.telegrammrobot.deleteMessage(chatId, msgId);
      text += `API Key: ${apikeyUser}\n`;
      text += `Secret Key: ${apiSecretUser}\n`;
      text += `Order size: ${buyOrderUser}\n`;
      text += `Status: ${robotStatusinactive}\n\n`;
      const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('payment-enter-api-datas'));
      lastMsgId.set(payload.chat.id, payload.message_id);
      await telegrammservice.setOneFieldRedis(payload.chat.id, 'payload', payload.message_id);
    }

    if (stateApi === 0) { // New user
      // connections.telegrammrobot.deleteMessage(chatId, msgId);
      text += `API Key: ${apikeyUser}\n`;
      text += `Secret Key: ${apiSecretUser}\n`;
      text += `Order size: ${buyOrderUser}\n`;
      text += `Status: ${robotStatusinactive}\n\n`;
      const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('payment-enter-api-datas'));
      lastMsgId.set(payload.chat.id, payload.message_id);      
    }

    if (stateApi === 1) { // Sub has ordered
      // console.log('chatId:', chatId, 'msgId:', msgId, 'typeof:', typeof msgId);
      // connections.telegrammrobot.deleteMessage(chatId, msgId);
      text += `API Key: ${apikeyUser}\n`;
      text += `Secret Key: ${apiSecretUser}\n`;
      text += `Order size: ${buyOrderUser}\n`;
      text += `Status: ${robotStatusinactive}\n\n`;
      text += '🕑️ We are waiting payment\n\n';
      const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('enter-api-datas'));
      lastMsgId.set(payload.chat.id, payload.message_id);
      await telegrammservice.setOneFieldRedis(payload.chat.id, 'payload', payload.message_id);
    }

    if (stateApi === 2) { // Sub has expired
      // connections.telegrammrobot.deleteMessage(chatId, msgId);
      text += `API Key: ${apikeyUser}\n`;
      text += `Secret Key: ${apiSecretUser}\n`;
      text += `Order size: ${buyOrderUser}\n`;
      text += `Status: ${robotStatusinactive}\n\n`;
      text += '⛔️ You subscription is <b>inactive</b>\n\n';
      const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('payment-enter-api-datas'));
      lastMsgId.set(payload.chat.id, payload.message_id);
      await telegrammservice.setOneFieldRedis(payload.chat.id, 'payload', payload.message_id);
    }

    if (stateApi === 3) { // Sub lives
      // connections.telegrammrobot.deleteMessage(chatId, msgId);
      const m = await trailingstoploss.getHowLongSubsLastApiTrading(30);
      text += `API Key: ${apikeyUser}\n`;
      text += `Secret Key: ${apiSecretUser}\n`;
      text += `Order size: ${buyOrderUser}\n`;
      text += `Status: ${robotStatus}\n\n`;
      text += '✅ You subscription is <b>active</b>\n';
      text += `⛔️ Expiration date: ${m}\n\n`;
      const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('enter-api-datas'));
      lastMsgId.set(payload.chat.id, payload.message_id);
      await telegrammservice.setOneFieldRedis(payload.chat.id, 'payload', payload.message_id);
    }
    await telegrammservice.setOneFieldRedis(msg.from.id, 'apidata', 'no');
    // let text = '💰 <b>FreeCryptoRobot - paid services</b>\n\n';
    // text += 'Using this telegram bot you can pay and configure paid services.';
    // const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('main-menu'));
    // lastMsgId.set(payload.chat.id, payload.message_id);
    // await telegrammservice.setOneFieldRedis(payload.chat.id, 'payload', payload.message_id);
  }
});

connections.telegrammrobot.onText(/\/help/, async (msg) => {
  if (!msg.from.is_bot) {
    let str = 'Commands:\n/start - show menu';
    str += '\n/menu - show menu';
    connections.telegrammrobot.sendMessage(msg.chat.id, str);
  }
});

connections.telegrammrobot.on('callback_query', async (msg) => {
  if (!msg.from.is_bot) {
    const foo = {
      id: msg.from.id,
      firstName: msg.from.first_name,
      isBot: msg.from.is_bot,
      languageCode: msg.from.language_code,
    };
    newuser.addNewUser(foo);

    const chatId = msg.message.chat.id;
    let msgId = lastMsgId.get(msg.message.chat.id);

    const trailingstoploss = new Trailingstoploss(connections, msg.from.id);

    const telegrammservice = new Telegrammservice(msg.message.chat.id, connections);

    if (typeof msgId === 'undefined') {
      const msgIdResult = await telegrammservice.getOneFieldRedis(chatId, 'payload');
      msgId = msgIdResult.get('payload');
    }

    let stateApi = 0;
    let stateTrailing = 0;
    // ====================================
    // Существует ли данный юзер в таблице подписок по трейлингу
    const resultExistence = await trailingstoploss.checkExistenceUser();
    if (Trailingstoploss.isEmptyObj(resultExistence)) { // Нет
      // Показываем выбор фри и добавляем его в таблицу
      // await trailingstoploss.addNewUser(msg.from.id);
      stateTrailing = 0;
    } else { // Да. Проверка подписок. Что по итогу выводить в меню
      // submenuTrailing = await trailingstoploss.chooseShowingBetweenTrialAndSub();
      stateTrailing = await trailingstoploss.chooseShowingBetweenTrialAndSub();
    }
    // ====================================

    // ====================================
    // Существует ли данный юзер в таблице подписок по API
    const resultExistenceApi = await apitrading.checkExistenceUser(msg.from.id);
    if (Apitrading.isEmptyObj(resultExistenceApi)) { // Нет
      // Показываем меню с вводом данных
      const r = await apitrading.checkLastPaymentUser(msg.from.id);
      const r2 = await apitrading.checkLastApiTrialUserStatus(msg.from.id);
      if (r === null) stateApi = 0; // Его нет действительно
      if (r === 0) stateApi = 1; // Он есть. Сделал запрос на кошель. Ждём оплаты
      if (r === 1) stateApi = 2; // Он есть. Но подписка истекла и он не сделал запрос
      // stateApi = 0;

      // if (msg.from.id === 402865625 || msg.from.id === 159856577) {
      console.log(msg.from.id);
      // Это триал версия
      if (r === null && r2 === null) stateApi = -4;

      // Триал активирован
      if (r === null && r2 === 0) stateApi = -3;

      // Триал закончен, но есть открытые сделки
      // или еще не проверен юзер на наличие таковых
      if (r === null && r2 === 1) stateApi = -2;

      // Триал закончен и проверка
      // на открытые сделки не производится
      // !!! НЕ заюыть, поменять значение поля
      // при вызове info_new: чтобы в таблице
      // менялось значение active_trial на `2`
      if (r === null && r2 === 2) stateApi = -1;
      // }
    } else { // Да. Показыем пустое меню
      stateApi = 3; // Пользователь с активной подпиской
    }
    // ====================================
    await telegrammservice.setOneFieldRedis(msg.from.id, 'stateapi', stateApi);

    try {
      switch (msg.data) {
        case 'trailing-main': {
          if (stateTrailing === 0) { // user is the first time
            let text = '🔔 <b>Signals With Stop-Loss Trailing</b>\n\n';
            text += 'Direct signals from telegram bot include buy signals, ';
            text += 'sell signals and stoploss movement signals of all Binance.com markets. ';
            text += '0.002 BTC for 30 days.\n\n';
            text += 'In this section you can check status and renew your subscriptions.';
            connections.telegrammrobot.deleteMessage(chatId, msgId);
            const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('stoploss-0'));
            lastMsgId.set(payload.chat.id, payload.message_id);
            await telegrammservice.setOneFieldRedis(payload.chat.id, 'payload', payload.message_id);
          }

          if (stateTrailing === 1) { // expired Free
            connections.telegrammrobot.deleteMessage(chatId, msgId);
            let text = '🔔 <b>Signals With Stop-Loss Trailing</b>\n\n';
            text += 'Direct signals from telegram bot include buy signals, ';
            text += 'sell signals and stoploss movement signals of all Binance.com markets. ';
            text += '0.002 BTC for 30 days.\n\n';
            text += 'In this section you can check status and renew your subscriptions.\n\n';
            text += '⛔ You subscription is <b>inactive</b>';
            const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('stoploss-1'));
            lastMsgId.set(payload.chat.id, payload.message_id);
            await telegrammservice.setOneFieldRedis(payload.chat.id, 'payload', payload.message_id);
          }

          if (stateTrailing === 2) {
            connections.telegrammrobot.deleteMessage(chatId, msgId);
            let text = 'That is `Stoploss Trailing` Menu.\n\n';
            text += '`Stoploss Trailing` subscription was ordered already.\n';
            text += 'We wait a payment from you. Pay subscription soon.\n\n';
            text += 'Choose what you need.';
            const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('stoploss-2'));
            lastMsgId.set(payload.chat.id, payload.message_id);
            await telegrammservice.setOneFieldRedis(payload.chat.id, 'payload', payload.message_id);
          }

          if (stateTrailing === 3) { // already Sub, 30 days
            connections.telegrammrobot.deleteMessage(chatId, msgId);
            const m = await trailingstoploss.getHowLongSubsLast(30);
            let text = 'You own next subscriptions already:\n';
            text += `\`Stoploss Trailing\`, final time is: ${m}\n\n`;
            text += 'More information about generall commands: /help';
            const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('stoploss-3'));
            lastMsgId.set(payload.chat.id, payload.message_id);
            await telegrammservice.setOneFieldRedis(payload.chat.id, 'payload', payload.message_id);
          }

          if (stateTrailing === 4) { // already Free, 7 days
            connections.telegrammrobot.deleteMessage(chatId, msgId);
            const d = await trailingstoploss.getHowLongSubsLast(7);
            // let text = 'You own next subscriptions already:\n';
            // text += `Free \`Stoploss Trailing\`, final time is: ${d}\n\n`;
            // text += 'More information about generall commands: /help';

            let text = '🔔 <b>Signals With Stop-Loss Trailing</b>\n\n';
            text += 'Direct signals from telegram bot include buy signals, ';
            text += 'sell signals and stoploss movement signals of all Binance.com markets. ';
            text += '0.002 BTC for 30 days.\n\n';
            text += 'In this section you can check status and renew your subscriptions.\n\n';
            text += '✅ You subscription is <b>active</b>\n';
            text += `⛔️ Expiration date: ${d}`;
            const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('stoploss-4'));
            lastMsgId.set(payload.chat.id, payload.message_id);
            await telegrammservice.setOneFieldRedis(payload.chat.id, 'payload', payload.message_id);
          }

          if (stateTrailing === 5) { // expired Sub
            connections.telegrammrobot.deleteMessage(chatId, msgId);
            let text = 'That is `Stoploss Trailing` Menu.\n\n';
            text += 'Your `Stoploss Trailing` has expired.\n\n';
            text += 'Choose what you need.';
            const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('stoploss-5'));
            lastMsgId.set(payload.chat.id, payload.message_id);
            await telegrammservice.setOneFieldRedis(msg.from.id, 'apidata', 'no');
            await telegrammservice.setOneFieldRedis(payload.chat.id, 'payload', payload.message_id);
          }
          await telegrammservice.setOneFieldRedis(msg.from.id, 'apidata', 'no');
          break;
        }

        case 'api-main': {
          const keysAndOrderResults = await trailingstoploss.getKeysAndSizeOrder();
          let apikeyUser = keysAndOrderResults.get('apikey') === '' ? 'N/A' : keysAndOrderResults.get('apikey');
          const apiSecretUser = keysAndOrderResults.get('secretkey') === '' ? 'N/A' : '****';
          let buyOrderUser = keysAndOrderResults.get('order');
          let buyOrderUserTF = true;
          if (keysAndOrderResults.get('order') === parseFloat('0.002')) {
            buyOrderUser = '0.002 BTC (value by defaults)';
            buyOrderUserTF = false;
          }
          if (keysAndOrderResults.get('order') === '') {
            buyOrderUser = 'N/A';
            buyOrderUserTF = false;
          }
          if (buyOrderUserTF) buyOrderUser += ' BTC';
          // const buyOrderUser = keysAndOrderResults.get('order') === parseFloat('0.002') ? '0.002 BTC (default)' : keysAndOrderResults.get('order');
          let apikeyUser1 = '';
          let apikeyUser2 = '';
          if (apikeyUser !== 'N/A') {
            apikeyUser1 = apikeyUser.substring(0, 4);
            apikeyUser2 = apikeyUser.substring(apikeyUser.length - 4);
            apikeyUser = `${apikeyUser1}****${apikeyUser2}`;
          }
          let robotStatus = '';
          let robotStatusinactive = '';
          let robotTrialStatus = '';
          let robotTrialStatusStart = '';
          if (apikeyUser !== '' && apikeyUser !== 'N/A'
              && apiSecretUser !== '' && apiSecretUser !== 'N/A') {
            robotStatus = 'robot is working';
            robotStatusinactive = 'Robot doesn\'t work. You have to pay subscription.';
            robotTrialStatus = 'TRIAL robot is working';
            robotTrialStatusStart = 'Activate your TRIAL';
          } else {
            robotStatus = 'API Key or Secret Key is empty';
            robotStatusinactive = 'API Key or Secret Key is empty';
            robotTrialStatus = 'API Key or Secret Key is empty';
            robotTrialStatusStart = 'API Key or Secret Key is empty';
          }

          let text = '🎯 <b>Binance API Trading Tool</b>\n\n';
          text += 'This tool fully automates the trading by signals, ';
          text += 'sending commands: to buy and to sell directly to your <a href="https://www.binance.com/?ref=20390268">Binance.com</a> ';
          text += 'account via API interface. 0.05 BTC for lifetime. Support: @FCR_INBOX_BOT\n\n';
          text += 'In this section you can check the status and renew your subscription. ';
          text += 'Also you can set up your API Keys and Order Size.\n\n';
          text += '<b>ATTENTION!!</b> <i>Login your Binance.com account then you have to create and to set up ';
          text += 'API KEY checkboxes:</i> <b>INFO</b> <i>and</i> <b>TRADING</b>.\n\n';

          if (stateApi === -4) { // New user
            // connections.telegrammrobot.deleteMessage(chatId, msgId);
            text += `API Key: ${apikeyUser}\n`;
            text += `Secret Key: ${apiSecretUser}\n`;
            text += `Order size: ${buyOrderUser}\n`;
            text += `Status: ${robotTrialStatusStart}\n\n`;
            const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('payment-enter-api-datas-t'));
            lastMsgId.set(payload.chat.id, payload.message_id);
            await telegrammservice.setOneFieldRedis(payload.chat.id, 'payload', payload.message_id);
          }

          if (stateApi === -3) { // New user
            // connections.telegrammrobot.deleteMessage(chatId, msgId);
            text += `API Key: ${apikeyUser}\n`;
            text += `Secret Key: ${apiSecretUser}\n`;
            text += `Order size: ${buyOrderUser}\n`;
            text += `Status: ${robotTrialStatus}\n\n`;
            const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('enter-api-datas'));
            lastMsgId.set(payload.chat.id, payload.message_id);
            await telegrammservice.setOneFieldRedis(payload.chat.id, 'payload', payload.message_id);
          }

          if (stateApi === -2) { // New user
            // connections.telegrammrobot.deleteMessage(chatId, msgId);
            text += `API Key: ${apikeyUser}\n`;
            text += `Secret Key: ${apiSecretUser}\n`;
            text += `Order size: ${buyOrderUser}\n`;
            text += `Status: ${robotStatusinactive}\n\n`;
            const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('payment-enter-api-datas'));
            lastMsgId.set(payload.chat.id, payload.message_id);
            await telegrammservice.setOneFieldRedis(payload.chat.id, 'payload', payload.message_id);
          }

          if (stateApi === -1) { // New user
            // connections.telegrammrobot.deleteMessage(chatId, msgId);
            text += `API Key: ${apikeyUser}\n`;
            text += `Secret Key: ${apiSecretUser}\n`;
            text += `Order size: ${buyOrderUser}\n`;
            text += `Status: ${robotStatusinactive}\n\n`;
            const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('payment-enter-api-datas'));
            lastMsgId.set(payload.chat.id, payload.message_id);
            await telegrammservice.setOneFieldRedis(payload.chat.id, 'payload', payload.message_id);
          }

          if (stateApi === 0) { // New user
            connections.telegrammrobot.deleteMessage(chatId, msgId);
            text += `API Key: ${apikeyUser}\n`;
            text += `Secret Key: ${apiSecretUser}\n`;
            text += `Order size: ${buyOrderUser}\n`;
            text += `Status: ${robotStatusinactive}\n\n`;
            const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('payment-enter-api-datas'));
            lastMsgId.set(payload.chat.id, payload.message_id);
          }

          if (stateApi === 1) { // Sub has ordered
            connections.telegrammrobot.deleteMessage(chatId, msgId);
            text += `API Key: ${apikeyUser}\n`;
            text += `Secret Key: ${apiSecretUser}\n`;
            text += `Order size: ${buyOrderUser}\n`;
            text += `Status: ${robotStatusinactive}\n\n`;
            text += '🕑️ We are waiting payment\n\n';
            const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('enter-api-datas'));
            lastMsgId.set(payload.chat.id, payload.message_id);
            await telegrammservice.setOneFieldRedis(payload.chat.id, 'payload', payload.message_id);
          }

          if (stateApi === 2) { // Sub has expired
            connections.telegrammrobot.deleteMessage(chatId, msgId);
            text += `API Key: ${apikeyUser}\n`;
            text += `Secret Key: ${apiSecretUser}\n`;
            text += `Order size: ${buyOrderUser}\n`;
            text += `Status: ${robotStatusinactive}\n\n`;
            text += '⛔️ You subscription is <b>inactive</b>\n\n';
            const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('payment-enter-api-datas'));
            lastMsgId.set(payload.chat.id, payload.message_id);
            await telegrammservice.setOneFieldRedis(payload.chat.id, 'payload', payload.message_id);
          }

          if (stateApi === 3) { // Sub lives
            connections.telegrammrobot.deleteMessage(chatId, msgId);
            const m = await trailingstoploss.getHowLongSubsLastApiTrading(30);
            text += `API Key: ${apikeyUser}\n`;
            text += `Secret Key: ${apiSecretUser}\n`;
            text += `Order size: ${buyOrderUser}\n`;
            text += `Status: ${robotStatus}\n\n`;
            text += '✅ You subscription is <b>active</b>\n';
            text += `⛔️ Expiration date: ${m}\n\n`;
            const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('enter-api-datas'));
            lastMsgId.set(payload.chat.id, payload.message_id);
            await telegrammservice.setOneFieldRedis(payload.chat.id, 'payload', payload.message_id);
          }
          await telegrammservice.setOneFieldRedis(msg.from.id, 'apidata', 'no');
          break;
        }

        case 'trailing-free': {
          if (stateTrailing === 0) {
            connections.telegrammrobot.deleteMessage(chatId, msgId);
            // const d = new Date().toISOString().split('T');
            // const d0 = d[0];
            // const d1 = d[1];
            // const d11 = d1.split(':');
            let text = '🔔 <b>Signals With Stop-Loss Trailing</b>\n\n';
            // text += `Remember!! Trial version is active just 7 days from current day: ${d0} ${d11[0]}:${d11[1]}.\n\n`;
            text += 'Your free trial is <b>active</b> now';
            const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('go-back-menu-from-stoploss'));
            lastMsgId.set(payload.chat.id, payload.message_id);
            await trailingstoploss.addNewUser(msg.from.id);
            await telegrammservice.setOneFieldRedis(payload.chat.id, 'payload', payload.message_id);
          } else {
            // let variants = ['activated', 'is', 'active'];
            // if (stateTrailing !== 4) variants = ['expired', 'was', 'active'];
            // let text = `Your Personal Trial Version was already ${variants[0]}.\n`;
            // text += `Remember!! Trial version ${variants[1]} ${variants[2]} just 7 days.\n\n`;
            // text += 'To start menu enter /start .';
            const d = new Date().toISOString().split('T');
            const d0 = d[0];
            let text = '🔔 <b>Signals With Stop-Loss Trailing</b>\n\n';
            text += 'Direct signals from telegram bot include buy signals, ';
            text += 'sell signals and stoploss movement signals of all Binance.com markets. ';
            text += '0.002 BTC for 30 days.\n\n';
            text += 'In this section you can check status and renew your subscriptions.\n\n';
            let varMenu = 'go-back-menu-from-stoploss';
            if (stateTrailing !== 4) {
              varMenu = 'stoploss-1';
              text += '⛔ You subscription is <b>inactive</b>';
            } else {
              varMenu = 'go-back-menu-from-stoploss';
              text += '✅ You subscription is <b>active</b>\n';
              text += `⛔️ Expiration date: ${d0}`;
            }
            const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get(varMenu));
            lastMsgId.set(payload.chat.id, payload.message_id);
            await telegrammservice.setOneFieldRedis(payload.chat.id, 'payload', payload.message_id);
          }
          await telegrammservice.setOneFieldRedis(msg.from.id, 'apidata', 'no');
          break;
        }

        case 'trailing-paid': {
          connections.telegrammrobot.deleteMessage(chatId, msgId);
          let text = 'Thank you! You will get Wallet to pay via Telegram soon.\n\n';
          text += 'To start menu enter /start .';
          const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('go-back-menu-from-stoploss'));
          lastMsgId.set(payload.chat.id, payload.message_id);
          await trailingstoploss.addNewPayment(msg.from.id, stateApi); // !!!!
          await telegrammservice.setOneFieldRedis(msg.from.id, 'apidata', 'no');
          await telegrammservice.setOneFieldRedis(payload.chat.id, 'payload', payload.message_id);
          break;
        }

        case 'api-paid': {
          console.log('stateApi():', stateApi);
          if (stateApi >= -1) { // Защита от старых сообщение, где было открыто 30 дней
            connections.telegrammrobot.deleteMessage(chatId, msgId);
            let text = '🎯 <b>Binance API Trading Tool</b>\n\n';
            text += '🛒 <b>Your order:</b> Binance API Trading Tool for life time\n\n';
            text += 'Subscription is activated automatically after payment of 0.05 BTC to the wallet specified in the details.\n\n';
            text += 'Attention: if the amount is less than the specified subscription is not activated.\n\n';
            const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('go-back-api-menu'));
            lastMsgId.set(payload.chat.id, payload.message_id);
            await apitrading.addNewPayment(msg.from.id, stateApi);
            // await apitrading.addNewUser(msg.from.id); // ?? Надо ли тут деативировать добаления нового пользователя
            await telegrammservice.setOneFieldRedis(msg.from.id, 'apidata', 'no');
            await telegrammservice.setOneFieldRedis(payload.chat.id, 'payload', payload.message_id);
          }
          break;
        }

        case 'api-trial': {
          connections.telegrammrobot.deleteMessage(chatId, msgId);
          let text = '🎯 <b>Binance API Trading Tool</b>\n\n';
          text += '🛒 <b>Your order:</b> Trial Binance API Trading Tool for 1 days\n\n';
          text += 'Subscription is activated automatically.\n\n';
          const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('go-back-api-menu'));
          lastMsgId.set(payload.chat.id, payload.message_id);
          await apitrading.addNewUser(msg.from.id);
          await telegrammservice.setOneFieldRedis(msg.from.id, 'apidata', 'no');
          await telegrammservice.setOneFieldRedis(payload.chat.id, 'payload', payload.message_id);
          break;
        }

        case 'api-key': {
          connections.telegrammrobot.deleteMessage(chatId, msgId);
          let text = '🎯 <b>Binance API Trading Tool</b>\n\n';
          text += 'Send your API Key\n\n';
          const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('go-back-api-menu'));
          lastMsgId.set(payload.chat.id, payload.message_id);
          await telegrammservice.setOneFieldRedis(msg.from.id, 'apidata', 'api-key');
          await telegrammservice.setOneFieldRedis(payload.chat.id, 'payload', payload.message_id);
          break;
        }
        case 'secret-key': {
          connections.telegrammrobot.deleteMessage(chatId, msgId);
          let text = '🎯 <b>Binance API Trading Tool</b>\n\n';
          text += 'Send your Secret Key\n\n';
          const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('go-back-api-menu'));
          lastMsgId.set(payload.chat.id, payload.message_id);
          await telegrammservice.setOneFieldRedis(msg.from.id, 'apidata', 'secret-key');
          await telegrammservice.setOneFieldRedis(payload.chat.id, 'payload', payload.message_id);
          break;
        }
        case 'order': {
          connections.telegrammrobot.deleteMessage(chatId, msgId);
          let text = '🎯 <b>Binance API Trading Tool</b>\n\n';
          text += 'Enter the size of the order in BTC. ';
          text += 'We recommend setting an order size equal to 10% of your deposit. ';
          text += 'The minimum order size is 0.002 BTC.\n\n';
          const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('go-back-api-menu'));
          lastMsgId.set(payload.chat.id, payload.message_id);
          await telegrammservice.setOneFieldRedis(msg.from.id, 'apidata', 'order');
          await telegrammservice.setOneFieldRedis(payload.chat.id, 'payload', payload.message_id);
          break;
        }
        case 'faq-tool': {
          connections.telegrammrobot.deleteMessage(chatId, msgId);
          let text = '🎯 <b>Binance API Trading Tool</b>\n\n';
          text += '<b>I have already paid, entered the keys, where can I see the results?</b>\n';
          text += 'You can see the results of trading on the right side of the stream or in your ';
          text += '<a href="https://www.binance.com?ref=20390268">Binance</a> account `Trade History` section.\n\n';
          text += '<b>I have already paid, entered the keys and nothing happens, what should I do?</b>\n';
          text += 'If you entered the correct keys and your subscription is active then the robot is trading.\n\n';
          text += '<b>What currency should be the account balance?</b>\n';
          text += 'The robot is set to increase the amount of Bitcoins, therefore the balance should be in ';
          text += 'Bitcoins and the BNB (to pay the commission).\n\n';
          text += '<b>What happens if I manually intervene in the trade?</b>\n';
          text += 'Nothing terrible will happen, but the algorithm works with a bundle of buying ';
          text += 'and selling and we can\'t guarantee the result. ';
          text += 'You\'d better have a separate account and allocate there the amount of which you are ready to trade.\n\n';
          text += '<b>What is the minimal balance required for bot operation?</b>\n';
          text += '0.002 (minimum order size) * 10 = 0.02 BTC is the minimal recommended deposit + BNB at your discretion.\n\n';
          text += '<b>How much BNB is needed on the balance sheet as a percentage of BTC to reduce the commission?</b>\n';
          text += '5% should be enough. BNB is a good investment.\n\n';
          text += '<b>How will a bot respond to a change in the balance up or down?</b>\n';
          text += 'The bot will not react in any way, you must control your balance by yourself, just make sure that ';
          text += 'the balance is not less than the size of the order * 10\n\n';
          text += '<b>Can I trade manually in other currency pairs?</b>\n';
          text += 'Yes, theoretically, you can trade manually, but we recommend you to create a new ';
          text += '<a href="https://www.binance.com?ref=20390268">Binance</a> account and don\'t trade manually on it.\n\n';
          text += '<b>If my subscription has ended, pending orders will remain hanging or will ';
          text += 'the bot cancel them before the subscription ends?</b>\n';
          text += 'After the end of the subscription, the bot will finish buying, all current transactions will be ';
          text += 'completed normally. The bot will not leave you in the shitcoins even if your subscription is over. ';
          text += 'The main task of the bot is as much as possible to preserve and increase the amount of Bitcoins.\n\n';
          const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('go-back-api-menu'));
          lastMsgId.set(payload.chat.id, payload.message_id);
          break;
        }
        case 'go-back-main-menu': {
          connections.telegrammrobot.deleteMessage(chatId, msgId);
          let text = '💰 <b>FreeCryptoRobot - paid services</b>\n\n';
          text += 'Using this telegram bot you can pay and configure paid services.';
          const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('main-menu'));
          lastMsgId.set(payload.chat.id, payload.message_id);
          await telegrammservice.setOneFieldRedis(msg.from.id, 'apidata', 'no');
          await telegrammservice.setOneFieldRedis(payload.chat.id, 'payload', payload.message_id);
          break;
        }
        case 'go-back-api-menu': {
          const keysAndOrderResults = await trailingstoploss.getKeysAndSizeOrder();
          let apikeyUser = keysAndOrderResults.get('apikey') === '' ? 'N/A' : keysAndOrderResults.get('apikey');
          const apiSecretUser = keysAndOrderResults.get('secretkey') === '' ? 'N/A' : '****';
          let buyOrderUser = keysAndOrderResults.get('order');
          let buyOrderUserTF = true;
          if (keysAndOrderResults.get('order') === parseFloat('0.002')) {
            buyOrderUser = '0.002 BTC (value by defaults)';
            buyOrderUserTF = false;
          }
          if (keysAndOrderResults.get('order') === '') {
            buyOrderUser = 'N/A';
            buyOrderUserTF = false;
          }
          if (buyOrderUserTF) buyOrderUser += ' BTC';
          // const buyOrderUser = keysAndOrderResults.get('order') === parseFloat('0.002') ? '0.002 BTC (default)' : keysAndOrderResults.get('order');
          let apikeyUser1 = '';
          let apikeyUser2 = '';
          if (apikeyUser !== 'N/A') {
            apikeyUser1 = apikeyUser.substring(0, 4);
            apikeyUser2 = apikeyUser.substring(apikeyUser.length - 4);
            apikeyUser = `${apikeyUser1}****${apikeyUser2}`;
          }
          let robotStatus = '';
          let robotStatusinactive = '';
          let robotTrialStatus = '';
          let robotTrialStatusStart = '';
          if (apikeyUser !== '' && apikeyUser !== 'N/A'
              && apiSecretUser !== '' && apiSecretUser !== 'N/A') {
            robotStatus = 'robot is working';
            robotStatusinactive = 'Robot doesn\'t work. You have to pay subscription.';
            robotTrialStatus = 'TRIAL robot is working';
            robotTrialStatusStart = 'Activate your TRIAL';
          } else {
            robotStatus = 'API Key or Secret Key is empty';
            robotStatusinactive = 'API Key or Secret Key is empty';
            robotTrialStatus = 'API Key or Secret Key is empty';
            robotTrialStatusStart = 'API Key or Secret Key is empty';
          }
          const getStateApiResult = await telegrammservice.getOneFieldRedis(msg.from.id, 'stateapi');
          const numberStateApi = getStateApiResult.get('stateapi');

          let text = '🎯 <b>Binance API Trading Tool</b>\n\n';
          text += 'This tool fully automates the trading by signals, ';
          text += 'sending commands: to buy and to sell directly to your <a href="https://www.binance.com/?ref=20390268">Binance.com</a> ';
          text += 'account via API interface. 0.05 BTC for lifetime. Support: @FCR_INBOX_BOT\n\n';
          text += 'In this section you can check the status and renew your subscription. ';
          text += 'Also you can set up your API Keys and Order Size.\n\n';
          text += '<b>ATTENTION!!</b> <i>Login your Binance.com account then you have to create and to set up ';
          text += 'API KEY checkboxes:</i> <b>INFO</b> <i>and</i> <b>TRADING</b>.\n\n';
          text += `API Key: ${apikeyUser}\n`;
          text += `Secret Key: ${apiSecretUser}\n`;
          text += `Order size: ${buyOrderUser}\n`;
          let variantApiMenu = 'enter-api-datas';

          if (numberStateApi === '0' || numberStateApi === '2' || numberStateApi === '-4') {
            if (stateApi === 2) {
              text += `Status: ${robotStatusinactive}\n\n`;
              text += '⛔️ You subscription is <b>inactive</b>\n\n';
            }
            variantApiMenu = 'payment-enter-api-datas';

            if (stateApi === -4) {
              text += `Status: ${robotTrialStatusStart}\n\n`;
              variantApiMenu = 'payment-enter-api-datas-t';             
            }
          } else {
            if (stateApi === 1) {
              text += `Status: ${robotStatusinactive}\n\n`;
              text += '🕑️ We are waiting payment\n\n';
              variantApiMenu = 'enter-api-datas';
            }
            if (stateApi === 3) {
              text += `Status: ${robotStatus}\n\n`;
              text += '✅ You subscription is <b>active</b>\n';
              const m = await trailingstoploss.getHowLongSubsLastApiTrading(30);
              text += `⛔️ Expiration date: ${m}\n\n`;
              variantApiMenu = 'enter-api-datas';
            }

            if (stateApi === -3) {
              text += `Status: ${robotTrialStatus}\n\n`;
            }

            if (stateApi === -2 || stateApi === -1) {
              text += `Status: ${robotStatusinactive}\n\n`;
              variantApiMenu = 'payment-enter-api-datas';
            }
          }

          connections.telegrammrobot.deleteMessage(chatId, msgId);
          const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get(variantApiMenu));
          lastMsgId.set(payload.chat.id, payload.message_id);
          await telegrammservice.setOneFieldRedis(msg.from.id, 'apidata', 'no');
          await telegrammservice.setOneFieldRedis(payload.chat.id, 'payload', payload.message_id);
          break;
        }

        case 'go-back-menu-from-stoploss': {
          connections.telegrammrobot.deleteMessage(chatId, msgId);
          const title = 'That is Main Menu. Choose what you need.';
          const payload = await telegrammservice.teleSendMessage(title, menu.fullmenu.get('main-menu'));
          lastMsgId.set(payload.chat.id, payload.message_id);
          await telegrammservice.setOneFieldRedis(msg.from.id, 'apidata', 'no');
          await telegrammservice.setOneFieldRedis(payload.chat.id, 'payload', payload.message_id);
          break;
        }
        default:
          break;
      }
    } catch (e) {
      console.log(e);
    }
  }
});

// Just to ping bot
connections.telegrammrobot.on('message', async (msg) => {
  if (!msg.from.is_bot) {
    const foo = {
      id: msg.from.id,
      firstName: msg.from.first_name,
      isBot: msg.from.is_bot,
      languageCode: msg.from.language_code,
    };
    newuser.addNewUser(foo);

    const telegrammservice = new Telegrammservice(msg.chat.id, connections);
    // const start = await telegrammservice.getOneFieldRedis(msg.from.id, 'start');
    // const msgapi = apiselect.get('api');
    const msgapiResult = await telegrammservice.getOneFieldRedis(msg.from.id, 'apidata');
    const msgapi = msgapiResult.get('apidata');

    const chatId = msg.chat.id;
    const msgId = lastMsgId.get(msg.chat.id);

    switch (msgapi) {
      case 'api-key': {
        // const telegrammservice = new Telegrammservice(msg.chat.id, connections);
        connections.telegrammrobot.deleteMessage(chatId, msgId);
        await telegrammservice.setOneFieldRedis(msg.from.id, 'apikey', msg.text);
        apiselect.set('api', 'no');
        const resultAll = await telegrammservice.getAllFieldsFromRedis(msg.from.id);
        let text = '';
        if (resultAll.get('apikey') !== null) {
          text = `Your API Key:\n${msg.text}\n\nSaved`;
          // connections.telegrammrobot.sendMessage(msg.chat.id, text);
          await apitrading.addEditFieldMySQL(msg.from.id, 'apikey', resultAll.get('apikey'));
        } else {
          text = `Your API Key:\n${msg.text}\n\nSaved`;
          // connections.telegrammrobot.sendMessage(msg.chat.id, text);
        }
        const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('go-back-api-menu'));
        lastMsgId.set(payload.chat.id, payload.message_id);
        await telegrammservice.setOneFieldRedis(payload.chat.id, 'payload', payload.message_id);
        break;
      }
      case 'secret-key': {
        // const telegrammservice = new Telegrammservice(msg.chat.id, connections);
        connections.telegrammrobot.deleteMessage(chatId, msgId);
        await telegrammservice.setOneFieldRedis(msg.from.id, 'secretkey', msg.text);
        apiselect.set('api', 'no');
        const resultAll = await telegrammservice.getAllFieldsFromRedis(msg.from.id);
        let text = '';
        if (resultAll.get('secretkey') !== null) {
          text = `Your Secret Key:\n${msg.text}\n\nSaved`;
          // connections.telegrammrobot.sendMessage(msg.chat.id, text);
          await apitrading.addEditFieldMySQL(msg.from.id, 'secretkey', resultAll.get('secretkey'));
        } else {
          text = `Your Secret Key:\n${msg.text}\n\nSaved`;
          // connections.telegrammrobot.sendMessage(msg.chat.id, text);
        }
        const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('go-back-api-menu'));
        lastMsgId.set(payload.chat.id, payload.message_id);
        await telegrammservice.setOneFieldRedis(payload.chat.id, 'payload', payload.message_id);
        break;
      }
      case 'order': {
        // const telegrammservice = new Telegrammservice(msg.chat.id, connections);
        let text = '';
        if (isFinite(msg.text) && parseFloat(msg.text) >= 0.002) {
          connections.telegrammrobot.deleteMessage(chatId, msgId);
          await telegrammservice.setOneFieldRedis(msg.from.id, 'order', msg.text);
          apiselect.set('api', 'no');
          const resultAll = await telegrammservice.getAllFieldsFromRedis(msg.from.id);
          if (resultAll.get('order') !== null) {
            text = `Your Order size is ${msg.text} BTC`;
            await apitrading.addEditFieldMySQL(msg.from.id, 'buy_order', resultAll.get('order'));
          } else {
            text = `Your Order size is ${msg.text} BTC`;
          }
        } else {
          connections.telegrammrobot.deleteMessage(chatId, msgId);
          text = '<b>Error</b>. The minimum order size is 0.002 BTC.\n';
          text += 'Go back to previus menu or enter value again.';
        }
        const payload = await telegrammservice.teleSendMessage(text, menu.fullmenu.get('go-back-api-menu'));
        lastMsgId.set(payload.chat.id, payload.message_id);
        await telegrammservice.setOneFieldRedis(payload.chat.id, 'payload', payload.message_id);
        break;
      }
      default:
        break;
    }

    if (!commands.includes(msg.text) && msgapi === 'no') {
      let text = 'Please use bot commands.\n';
      text += 'If you are into API menu so use buttons.\n\n';
      text += 'More information about generall commands: /help';
      connections.telegrammrobot.sendMessage(msg.chat.id, text);
    }
  }
});

setInterval(() => {
  connections.mysqldb.query('SELECT 1', (error, results) => {
    // console.log(results);
  });
  const sql = 'SELECT * FROM public.monitoringsbinance_mar_total_sim WHERE (container = $1) LIMIT 1;';
  connections.postgre.query(sql, ['1'], () => {
    console.log('Check postgre connect.');
  });
}, 1 * 2 * 60 * 1000);

setInterval(() => {
  trailingstoplossMonitoring.monitorTrialUsers((error) => {
    if (error) console.log(error);
    // console.log('trial', trailingstoplossMonitoring.listTrialUsers);
  });

  trailingstoplossMonitoring.monitorSubUsers((error) => {
    if (error) console.log(error);
    // console.log('sub', trailingstoplossMonitoring.listTrialUsers);
  });

  apitrading.monitorApiTraidingSubUsers((error) => {
    if (error) console.log(error);

    console.log('api every 5 minutes', apitrading.listApiSubUsers);
    // console.log('sub', trailingstoplossMonitoring.listTrialUsers);
  });

  apitrading.monitorApiTraidingSubExpiredTrialUsers((error) => {
    if (error) console.log(error);

    console.log('sell operation for trial api users', apitrading.listApiSubTrialExpiredUsers);
  });
}, 1 * 60 * 60 * 1000);
// }, 1 * 5 * 60 * 1000);

setInterval(() => {
  console.log('Checking expired users: API subs');
  trailingstoplossMonitoring.disableUserStoplossExpired();
  apitrading.disableUserExpiredSubs();
  apitrading.disableUserExpiredSubsTrial();
  // apitrading.disableUserExpiredSubsForSell();
}, 2 * 60 * 60 * 1000);

setInterval(() => {
  console.log('Checking expired users: API trial');
  apitrading.disableUserExpiredSubsTrial();
}, 1 * 1 * 60 * 1000);

setInterval(() => {
  console.log('Checking expired users SELL: API trial');
  apitrading.disableUserExpiredSubsForSell();
}, 1 * 3 * 60 * 1000);

// setInterval(() => {
//   console.log('Checking expired users: API trial');
//   trailingstoplossMonitoring.disableUserStoplossExpired();
//   apitrading.disableUserExpiredSubsTrial();
// }, 4 * 60 * 60 * 1000);