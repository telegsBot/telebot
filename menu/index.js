class Menu {
  constructor() {
    this.fullmenu = new Map();

    this.createFullMenu();
    return this;
  }

  createFullMenu() {
    let arr1 = [];
    let arr2 = [];

    // arr1 = ['🔔 Signals With Stop-Loss Trailing', '🎯 Binance API Trading Tool'];
    arr1 = ['🎯 Binance API Trading Tool'];
    arr2 = ['api-main'];
    this.fullmenu.set('main-menu', Menu.createSubMenu(arr1, arr2));

    arr1 = ['🕑 Get Free Trial For 7 Days', '◀️ Main Menu'];
    arr2 = ['trailing-free', 'go-back-main-menu'];
    this.fullmenu.set('stoploss-0', Menu.createSubMenu(arr1, arr2));

    arr1 = ['🕑 Buy Access For 30 Days', '◀️ Main Menu'];
    arr2 = ['trailing-paid', 'go-back-main-menu'];
    this.fullmenu.set('stoploss-1', Menu.createSubMenu(arr1, arr2));

    arr1 = ['◀️ Main Menu'];
    arr2 = ['go-back-main-menu'];
    this.fullmenu.set('stoploss-2', Menu.createSubMenu(arr1, arr2));

    arr1 = ['◀️ Main Menu'];
    arr2 = ['go-back-main-menu'];
    this.fullmenu.set('stoploss-3', Menu.createSubMenu(arr1, arr2));

    arr1 = ['◀️ Main Menu'];
    arr2 = ['go-back-main-menu'];
    this.fullmenu.set('stoploss-4', Menu.createSubMenu(arr1, arr2));

    arr1 = ['🕑 Buy Access For 30 Days', '◀️ Main Menu'];
    arr2 = ['trailing-paid', 'go-back-main-menu'];
    this.fullmenu.set('stoploss-5', Menu.createSubMenu(arr1, arr2));

    arr1 = ['◀️ Main Menu'];
    arr2 = ['go-back-menu-from-stoploss'];
    this.fullmenu.set('go-back-menu-from-stoploss', Menu.createSubMenu(arr1, arr2));

    arr1 = ['🕑 Buy Access For 30 Days', '🔑 Edit API Key', '🔐 Edit Secret Key', '💰 Edit Order Size in BTC', '❓ FAQ'];
    arr2 = ['api-paid', 'api-key', 'secret-key', 'order', 'faq-tool'];
    this.fullmenu.set('payment-enter-api-datas', Menu.createSubMenu(arr1, arr2));

    arr1 = ['🕑 Trial For 1 Day', '🔑 Edit API Key', '🔐 Edit Secret Key', '💰 Edit Order Size in BTC', '❓ FAQ'];
    arr2 = ['api-trial', 'api-key', 'secret-key', 'order', 'faq-tool'];
    this.fullmenu.set('payment-enter-api-datas-t', Menu.createSubMenu(arr1, arr2));

    // arr1 = ['🕑 Trial For 1 Day', '🔑 Edit API Key', '🔐 Edit Secret Key', '💰 Edit Order Size in BTC'];
    // arr2 = ['api-trial', 'api-key', 'secret-key', 'order'];
    // this.fullmenu.set('payment-enter-api-datas-t', Menu.createSubMenu(arr1, arr2));

    arr1 = ['🔑 Edit API Key', '🔐 Edit Secret Key', '💰 Edit Order Size in BTC', '❓ FAQ'];
    arr2 = ['api-key', 'secret-key', 'order', 'faq-tool'];
    this.fullmenu.set('enter-api-datas', Menu.createSubMenu(arr1, arr2));

    arr1 = ['◀️ Main Menu'];
    arr2 = ['go-back-api-menu'];
    this.fullmenu.set('go-back-api-menu', Menu.createSubMenu(arr1, arr2));

    return null;
  }

  static createSubMenu(text, callbacks) {
    const inlineKeyboardArr = [];
    text.forEach((dataset, index) => {
      const arr = [];
      const foo = {};
      foo.text = dataset;
      foo.callback_data = callbacks[index];
      arr.push(foo);
      inlineKeyboardArr.push(arr);
    });

    return {
      parse_mode: 'html',
      disable_web_page_preview: true,
      reply_markup: JSON.stringify({
        inline_keyboard: inlineKeyboardArr,
      }),
    };
  }
}

module.exports = Menu;