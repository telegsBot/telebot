class Newuser {
  constructor(connections) {
    this.mysql = connections.mysqldb;
    this.mysql.connect();

    return this;
  }

  addNewUser(foo) {
    const sql = 'INSERT IGNORE INTO `god_db1`.`users_telegram` SET ?';
    let activeUser = true;
    if (foo.isBot) activeUser = false;
    const datas = {
      userid: foo.id,
      username: foo.firstName,
      datereg: new Date(),
      active: activeUser,
      blacklist: false,
      lang: foo.languageCode,
      is_bot: foo.isBot,
    };
    this.mysql.query(sql, datas, (error) => {
      if (error) throw error;

      // console.log(results.insertId);
    });
  }
}

module.exports = Newuser;