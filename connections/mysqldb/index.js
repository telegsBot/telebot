const mysql = require('mysql');

class Mysqldb {
  constructor() {
    this.connection = Mysqldb.connectToDb();

    return this.connection;
  }

  static connectToDb() {
    const connection = new mysql.createConnection({
      host: process.env.MYSQL_HOST,
      user: process.env.MYSQL_USER,
      password: process.env.MYSQL_PASSWORD,
      database: process.env.MYSQL_DB,
    });

    // connection.query('SET SESSION wait_timeout = 604800');
    return connection;
  }
}

module.exports = Mysqldb;