class Tablestopllsmonitoring {
  constructor(connections) {
    this.mysql = connections.mysqldb;

    return this;
  }

  monitor() {
    const sql = `SELECT ( DATE_ADD(NOW(), INTERVAL 3 HOUR) < DATE_ADD(${interval}) ) AS diff FROM \`god_db1\`.\`trailing_stoploss\` WHERE userid = 142545448`;
    this.mysql.query(sql, (error, results) => {
      if (error) {
        // console.log(error.sqlMessage);
        reject(error.sqlMessage);
      }

      // console.log(results[0].diff);
      resolve(results[0].diff);
    });
  }
}

module.exports = Tablestopllsmonitoring;