import Trailingstoploss from '../index';

class Gettrailusers extends Trailingstoploss {
  monitorTrailUsers() {
    const sql = 'SELECT * FROM `god_db1`.`trailing_stoploss` WHERE DATE_ADD(NOW(), INTERVAL 3 HOUR) < DATE_ADD(DATE_TRIAL, INTERVAL 7 DAY);';
    super.mysql.query(sql, (error, results) => {
      if (error) {
        console.log(error.sqlMessage);
      }

      return results;
    });
  }
}

module.exports = Gettrailusers;