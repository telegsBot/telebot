const { TelegramClient } = require('messaging-api-telegram');

const clientTeleg = TelegramClient.connect(process.env.TELEGRAMM_CONNECT1);

const Binance = require('node-binance-api');
const request = require('request');

class Apitrading {
  constructor(connections, userId = '') {
    this.redissubs = connections.redissubs;
    this.mysql = connections.mysqldb;
    this.postgre = connections.postgre;

    this.userId = userId;

    this.listApiSubUsers = [];
    this.listApiSubTrialExpiredUsers = [];

    return this;
  }

  monitoringBuySell() {
    // const attemps = [2, 3];
    const attemps = [2];
    Object.keys(this.redissubs).forEach((key) => {
      console.log('Redis is %s, typeof key of array is %s', key, typeof key);

      if (key !== 'redis0') {
        this.redissubs[key].subscribe('buy', 'sell', () => {
          this.redissubs[key].on('message', (channel, message) => {
            let varTraiding = 0;
            if (key === 'redis1') console.log(channel, 'i-1');
            if (key === 'redis2') console.log(channel, 'i-2');
            if (key === 'redis3') console.log(channel, 'i-3');
            if (key === 'redis4') console.log(channel, 'i-4');
            if (key === 'redis5') console.log(channel, 'i-5');
            if (key === 'redis6') {
              console.log(channel, 'i-6 experemental');
              varTraiding = 1;
            }
            switch (channel) {
              case 'buy':
                if (this.listApiSubUsers.length !== 0) {
                  this.listApiSubUsers.forEach(async (dataset) => {
                    if (dataset.varianttraiding === varTraiding) {
                      const binance = new Binance().options({
                        APIKEY: dataset.apikey,
                        APISECRET: dataset.secretkey,
                        reconnect: true,
                        recvWindow: 5000,
                        useServerTime: true,
                        verbose: true,
                        test: false,
                      });

                      try {
                        await this.buy(message, dataset, binance);
                      } catch (e) {
                        console.log(e);
                        const obj = JSON.parse(e);
                        const codeError = obj.code;
                        const msgError = obj.msg;
                        let nextStr = '';
                        console.log('Type of errors:', typeof codeError, typeof msgError, dataset.userid);
                        if (msgError === 'Account has insufficient balance for requested action.') {
                          let amountBuyBalance = await Apitrading.getAmountOfMarketFromBinance('BTCBTC', binance);
                          amountBuyBalance = parseFloat(amountBuyBalance);

                          if (amountBuyBalance >= parseFloat(dataset.buyorder)) {
                            attemps.forEach(async (attemp) => {
                            // for (const attemp of attemps) {
                              try {
                                if (nextStr === '') {
                                  nextStr = await this.buy(message, dataset, binance);
                                }
                              } catch (e2) {
                                nextStr = '';
                                console.log(`Error. Attemp #${attemp}`);
                              }
                            });
                            // });
                          } else {
                            const buyData = JSON.parse(message);
                            const market = buyData.Symbol;
                            const action = buyData.Action;

                            const myObjErr = {
                              code: '-2',
                              msg: 'Account has insufficient balance for requested action BTC',
                            };

                            const strMyObjErr = JSON.stringify(myObjErr);
                            Apitrading.sendBuySellErrorViaTelegramm(strMyObjErr, market, action, dataset.userid);
                          }
                        }
                      }
                    }
                  });
                }
                break;
              case 'sell':
                if (this.listApiSubUsers.length !== 0) {
                  this.listApiSubUsers.forEach(async (dataset) => {
                    if (dataset.varianttraiding === varTraiding) {
                      const sellBinance1 = new Binance().options({
                        APIKEY: dataset.apikey,
                        APISECRET: dataset.secretkey,
                        reconnect: true,
                        recvWindow: 5000,
                        useServerTime: true,
                        verbose: true,
                        test: false,
                      });
                      await this.sell(message, dataset, sellBinance1);
                    }
                  });
                }

                if (this.listApiSubTrialExpiredUsers.length !== 0) {
                  this.listApiSubTrialExpiredUsers.forEach(async (dataset) => {
                    if (dataset.varianttraiding === varTraiding) {
                      const sellBinance2 = new Binance().options({
                        APIKEY: dataset.apikey,
                        APISECRET: dataset.secretkey,
                        reconnect: true,
                        recvWindow: 5000,
                        useServerTime: true,
                        verbose: true,
                        test: false,
                      });
                      await this.sell(message, dataset, sellBinance2);
                    }
                  });
                }
                break;
              default:
                break;
            }
          });
        });
      }
    });
  }

  // msg -- data from redis channel
  // foo -- data from mysql
  buy(msg, foo, buyUserBinance) {
    if (foo.apikey === '' || foo.secretkey === '' || !isFinite(foo.buyorder) || parseFloat(foo.buyorder) < 0.001) {
      return new Promise((resolve) => {
        const buyData = JSON.parse(msg);
        const market = buyData.Symbol;
        const action = buyData.Action;

        console.log('Error production buy:', market, 'Error: empty API or Secret Key. Apikey', foo.apikey, 'Secret key:', foo.secretkey, 'Order size:', foo.buyorder, foo.userid);
        const myObjErr = {
          code: '-1',
          msg: 'Empty keys or order size',
        };

        const strMyObjErr = JSON.stringify(myObjErr);
        Apitrading.sendBuySellErrorViaTelegramm(strMyObjErr, market, action, foo.userid);
        resolve(null);
      });
    }

    return new Promise((resolve, reject) => {
      const buyData = JSON.parse(msg);
      const market = buyData.Symbol;
      const raz = buyData.Raz;
      const firstAsksRate = buyData.Buyprice;
      const action = buyData.Action;
      // const stoploss = buyData.Stoploss;
      const stepsize = buyData.Stepsize;

      console.log('BUY: market is %s. UserId is %s.', market, foo.userid);
      console.log('BUY:', buyData, foo.userid);

      let selectQuantitySellProd = 0;
      const quantitySellProd = parseFloat(foo.buyorder) / parseFloat(firstAsksRate);
      selectQuantitySellProd = buyUserBinance.roundStep(parseFloat(quantitySellProd), stepsize);
      // quantitySellProd = quantitySellProd.toFixed(raz);
      // quantitySellProd = parseFloat(quantitySellProd);
      console.log('BUY: Quantitys:', quantitySellProd, selectQuantitySellProd, market, raz, firstAsksRate, foo.userid);

      if (parseFloat(quantitySellProd) !== 0.0) {
        // buyUserBinance.useServerTime(() => {
          buyUserBinance.marketBuy(market, selectQuantitySellProd, (error, response) => {
            if (error) {
              console.log('Error production buy:', market, 'Error:', error.body, typeof error.body, foo.apikey, foo.secretkey, foo.userid);

              Apitrading.sendBuySellErrorViaTelegramm(error.body, market, action, foo.userid);

              reject(error.body);
            }

            if (!error) {
              console.log('Buy production API Traiding. Status is success. Market:', market, foo.userid);
              console.log('Buy production API Traiding. MarketBuy response is', response, foo.userid);
              let totalSum = 0.0;
              response.fills.forEach((dataset) => {
                totalSum = parseFloat(totalSum) + parseFloat(dataset.qty);
              });

              totalSum = totalSum.toFixed(raz);

              const dateInsertSim = new Date();
              const orderId = response.orderId;

              if (foo.userid === 142545448) {
                const sqlNtotal = 'INSERT INTO public.bsnew_sim_binance_total (uuidord, market, timeutcbuy, typeordbuy, quantitybuy, ratebuy, bodybuy, notebuy, statusbuy, timeutcsell, typeordsell, quantitysell, ratesell, bodysell, notesell, statussell, persent, active, note, status, stoploss) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21);';
                const queryArrI2 = [orderId, market, dateInsertSim, 'BUY', totalSum, response.fills[0].price, response, '', 'OK', dateInsertSim, '', 0.0, 0.0, '{}', '', '', 0.0, true, '', '', 0.0];
                this.postgre.query(sqlNtotal, queryArrI2, (errI1) => {
                  if (errI1) console.log(market, errI1);
                });
              }

              const sqlAll = 'INSERT INTO public.api_trading_users (uuidord, market, timeutcbuy, typeordbuy, quantitybuy, ratebuy, bodybuy, notebuy, statusbuy, timeutcsell, typeordsell, quantitysell, ratesell, bodysell, notesell, statussell, persent, active, note, status, stoploss, userid) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22);';
              const queryAll = [orderId, market, dateInsertSim, 'BUY', totalSum, response.fills[0].price, response, '', 'OK', dateInsertSim, '', 0.0, 0.0, '{}', '', '', 0.0, true, '', '', 0.0, foo.userid];
              this.postgre.query(sqlAll, queryAll, (errI2) => {
                if (errI2) console.log(market, errI2);
              });
              resolve('buy success');
            }
          });
        // });
      } else {
        console.log('quantitySellProd is 0', market, foo.buyorder, foo.userid);
        resolve('quantitySellProd');
      }
    });
  }

  // msg -- data from redis channel
  // foo -- data from mysql
  async sell(msg, foo, sellUserBinance) {
    const buyData = JSON.parse(msg);
    const market = buyData.Symbol;
    const action = buyData.Action;
    const raz = buyData.Raz;
    const stepsize = buyData.Stepsize;

    if (foo.apikey === '' || foo.secretkey === '') {
      return new Promise((resolve) => {
        console.log('Error production sell:', market, 'Error: empty API or Secret Key. Apikey', foo.apikey, 'Secret key:', foo.secretkey);
        const myObjErr = {
          code: '-1',
          msg: 'Empty keys',
        };

        const strMyObjErr = JSON.stringify(myObjErr);
        Apitrading.sendBuySellErrorViaTelegramm(strMyObjErr, market, action, foo.userid);
        resolve(null);
      });
    }

    let amountSell = await this.getAmountOfSellingCoinsForUser(market, foo.userid);
    const amountSellBalance = await Apitrading.getAmountOfMarketFromBinance(market, sellUserBinance);
    return new Promise((resolve, reject) => {
      if (amountSell !== 'no aviable amount coins' && amountSell !== 0.0 && parseFloat(amountSellBalance) !== 0.0 && amountSellBalance !== null) {
        if (parseFloat(amountSellBalance) < parseFloat(amountSell)) {
          console.log('ERROR AMOUNT SELL:', foo.userid, amountSellBalance, amountSell);
          amountSell = parseFloat(amountSellBalance);
        }
        amountSell = sellUserBinance.roundStep(parseFloat(amountSell), stepsize);
        // const flags = { type: 'MARKET' };
        // sellUserBinance.marketSell(market, amountSell, flags, (error, response) => {
        sellUserBinance.marketSell(market, amountSell, (error, response) => {
          console.log('Sell production. Start. Market is %s . UserId is %s .', market, foo.userid, foo.apikey, foo.secretkey);
          if (error) {
            console.log('Error production sell:', market, 'Error:', error.body, amountSell, typeof error.body);
            // Experemnt 10.02 HOME
            const dateInsertSim = new Date();
            if (foo.userid === 142545448) {
              const sqlSelect = 'SELECT * FROM public.bsnew_sim_binance_total WHERE (market = $1 AND typeordbuy = $2 AND typeordsell = $3) ORDER BY timeutcbuy DESC LIMIT 1';
              this.postgre.query(sqlSelect, [market, 'BUY', ''], (errS, result) => {
                if (errS) console.log(market, errS);

                if (result.rowCount === 1) {
                  result.rows.forEach((dataset1) => {
                    const sqlUpdate = 'UPDATE public.bsnew_sim_binance_total SET timeutcsell=$1, typeordsell=$2, quantitysell=$3, ratesell=$4, bodysell=$5, notesell=$6, statussell=$7 WHERE (uuidord = $8);';
                    this.postgre.query(sqlUpdate, [dateInsertSim, 'SELL', amountSell, dataset1.ratebuy, '{}', '', 'OK', dataset1.uuidord], () => {
                    });
                  });
                  // resolve('sell success');
                } else {
                  console.log(market, '(): error in UPDATE public.bsnew_sim_binance_total');
                  // resolve('result.rowCoun is 0');
                }
              });
            }

            const sqlSelect1 = 'SELECT * FROM public.api_trading_users WHERE (market = $1 AND typeordbuy = $2 AND typeordsell = $3 AND userid = $4) ORDER BY timeutcbuy DESC LIMIT 1';
            this.postgre.query(sqlSelect1, [market, 'BUY', '', foo.userid], (errS, result) => {
              if (errS) console.log(market, errS);

              if (!errS) {
                if (result.rowCount === 1) {
                  result.rows.forEach((dataset1) => {
                    const sqlUpdate = 'UPDATE public.api_trading_users SET timeutcsell=$1, typeordsell=$2, quantitysell=$3, ratesell=$4, bodysell=$5, notesell=$6, statussell=$7 WHERE (uuidord = $8 AND userid = $9);';
                    this.postgre.query(sqlUpdate, [dateInsertSim, 'SELL', amountSell, dataset1.ratebuy, '{}', '', 'OK', dataset1.uuidord, foo.userid], () => {
                    });
                  });
                  resolve('sell success');
                } else {
                  console.log(market, '(): error in UPDATE public.api_trading_users');
                  resolve('result.rowCoun is 0');
                }
              } else {
                resolve('result.rowCoun is 0');
              }
            });
            // Experemnt 10.02 END

            Apitrading.sendBuySellErrorViaTelegramm(error.body, market, action, foo.userid);
            reject(error.body);
          }

          if (!error) {
            console.log('Sell production User API Traiding. Status is success. Market:', market);
            console.log('Sell production User API Traiding. MarketSell response is', response);
            let totalSumSell = 0.0;
            response.fills.forEach((dataset) => {
              totalSumSell = parseFloat(totalSumSell) + parseFloat(dataset.qty);
            });

            totalSumSell = totalSumSell.toFixed(raz);

            const dateInsert = new Date();
            console.log('Time stoploss(msg #2) ', market, dateInsert);

            // this.sendApiMessageViaTelegramm(market, response.fills[0].price, action, '', foo.userid);

            const dateInsertSim = new Date();
            if (foo.userid === 142545448) {
              const sqlSelect = 'SELECT * FROM public.bsnew_sim_binance_total WHERE (market = $1 AND typeordbuy = $2 AND typeordsell = $3) ORDER BY timeutcbuy DESC LIMIT 1';
              this.postgre.query(sqlSelect, [market, 'BUY', ''], (errS, result) => {
                if (errS) console.log(market, errS);

                if (result.rowCount === 1) {
                  result.rows.forEach((dataset1) => {
                    const sqlUpdate = 'UPDATE public.bsnew_sim_binance_total SET timeutcsell=$1, typeordsell=$2, quantitysell=$3, ratesell=$4, bodysell=$5, notesell=$6, statussell=$7 WHERE (uuidord = $8);';
                    this.postgre.query(sqlUpdate, [dateInsertSim, 'SELL', totalSumSell, response.fills[0].price, response, '', 'OK', dataset1.uuidord], () => {
                    });
                  });
                  // resolve('sell success');
                } else {
                  console.log(market, '(): error in UPDATE public.bsnew_sim_binance_total');
                  // resolve('result.rowCoun is 0');
                }
              });
            }

            const sqlSelect1 = 'SELECT * FROM public.api_trading_users WHERE (market = $1 AND typeordbuy = $2 AND typeordsell = $3 AND userid = $4) ORDER BY timeutcbuy DESC LIMIT 1';
            this.postgre.query(sqlSelect1, [market, 'BUY', '', foo.userid], (errS, result) => {
              if (errS) console.log(market, errS);

              if (!errS) {
                if (result.rowCount === 1) {
                  result.rows.forEach((dataset1) => {
                    const sqlUpdate = 'UPDATE public.api_trading_users SET timeutcsell=$1, typeordsell=$2, quantitysell=$3, ratesell=$4, bodysell=$5, notesell=$6, statussell=$7 WHERE (uuidord = $8 AND userid = $9);';
                    this.postgre.query(sqlUpdate, [dateInsertSim, 'SELL', totalSumSell, response.fills[0].price, response, '', 'OK', dataset1.uuidord, foo.userid], () => {
                    });
                  });
                  resolve('sell success');
                } else {
                  console.log(market, '(): error in UPDATE public.api_trading_users');
                  resolve('result.rowCoun is 0');
                }
              } else {
                resolve('result.rowCoun is 0');
              }
            });

            sellUserBinance.useServerTime(() => {
              sellUserBinance.balance(() => {});
            });
          }
        });
      } else {
        console.log(`no aviable datas for ${market} and user ${foo.userid} and ${amountSell} and ${amountSellBalance}`);
        resolve('no aviable amount coins');
      }
    });
  }

  static getUserBalance(binanceObj) {
    return new Promise((resolve) => {
      binanceObj.useServerTime(() => {
        binanceObj.balance((error) => {
          if (error) {
            console.log(error.body);
            resolve('balance error');
          }

          resolve('ok');
        });
      });
    });
  }

  // Получаем кол-во монет конкретного пользователя
  // Для того что бы отправить это кол-во на продажу
  getAmountOfSellingCoinsForUser(market, userid) {
    return new Promise((resolve, reject) => {
      const sql = 'SELECT quantitybuy FROM public.api_trading_users WHERE (market = $1 AND typeordbuy = $2 AND typeordsell = $3 AND userid = $4) ORDER BY timeutcbuy DESC LIMIT 1;';
      // const sql = 'SELECT quantitybuy FROM public.bsnew_sim_binance_total WHERE (market = $1 AND typeordbuy = $2 AND typeordsell = $3) ORDER BY timeutcbuy DESC LIMIT 1';
      this.postgre.query(sql, [market, 'BUY', '', userid], (err, result) => {
        if (err) {
          console.log(market, err);
          reject(err);
        }

        let quantitybuy = 0.0;
        if (result.rowCount === 1) {
          result.rows.forEach((dataset) => {
            quantitybuy = parseFloat(dataset.quantitybuy);
            resolve(quantitybuy);
          });
        } else {
          console.log(`no aviable datas for ${market} and user ${userid} and ${quantitybuy}`);
          resolve('no aviable amount coins');
        }
      });
    });
  }

  static getAmountOfMarketFromBinance(market, binanceObj) {
    return new Promise((resolve) => {
      binanceObj.useServerTime(() => {
        binanceObj.balance((error, balances) => {
          if (error) {
            console.log(error.body);
            resolve(null);
          }

          let shortMarketName = '';
          if (market !== 'BTCBTC') {
            shortMarketName = market.replace('BTC', '');
          } else {
            shortMarketName = 'BTC';
          }

          Object.keys(balances).forEach((key) => {
            if (key.toString() === shortMarketName) {
              const balance = balances[key].available;
              resolve(balance);
            }
          });
        });
      });
    });
  }

  monitorApiTraidingSubUsers(callback) {
    this.listApiSubUsers = [];
    // const sql = 'SELECT * FROM `god_db1`.`api_trading` WHERE ( DATE_ADD(NOW(), INTERVAL 3 HOUR) <= DATE_ADD(DATE_SUB, INTERVAL 30 DAY) AND `active` = 1);';
    let sql = 'SELECT * FROM `god_db1`.`api_trading` WHERE ';
    sql += '( DATE_ADD(NOW(), INTERVAL 3 HOUR) <= DATE_ADD(DATE_SUB, INTERVAL 30 DAY) AND `active` = 1 OR';
    sql += '( DATE_ADD(NOW(), INTERVAL 1 HOUR) <= DATE_ADD(date_trial, INTERVAL 25 HOUR) AND `active_trial` = 0 AND `apikey` <> \'\' AND `secretkey` <> \'\')';
    sql += ');';
    this.mysql.query(sql, (error, results) => {
      if (error) {
        console.log(error.sqlMessage);
        return callback(error.sqlMessage);
      }

      Object.keys(results).forEach((key) => {
        const foo = {
          userid: results[key].userid,
          apikey: results[key].apikey,
          secretkey: results[key].secretkey,
          buyorder: results[key].buy_order,
          varianttraiding: results[key].variant_of_traiding,
        };
        this.listApiSubUsers.push(foo);
      });

      return callback(null, results);
    });
  }

  // Получаем тех у кого был триал, но сейчас
  // этот триал истёк. Нужно для того,
  // чтобы у таких челов продать отктырые ордера
  monitorApiTraidingSubExpiredTrialUsers(callback) {
    this.listApiSubTrialExpiredUsers = [];
    const sql = 'SELECT * FROM `god_db1`.`api_trading` WHERE ( DATE_ADD(NOW(), INTERVAL 1 HOUR) > DATE_ADD(DATE_TRIAL, INTERVAL 25 HOUR) AND `active_trial` = 1);';
    this.mysql.query(sql, (error, results) => {
      if (error) {
        console.log(error.sqlMessage);
        return callback(error.sqlMessage);
      }

      Object.keys(results).forEach((key) => {
        const foo = {
          userid: results[key].userid,
          apikey: results[key].apikey,
          secretkey: results[key].secretkey,
          buyorder: results[key].buy_order,
          varianttraiding: results[key].variant_of_traiding,
        };
        this.listApiSubTrialExpiredUsers.push(foo);
      });

      return callback(null, results);
    });
  }


  disableUserExpiredSubs() {
    const sql = 'UPDATE `god_db1`.`api_trading` SET `active` = 0 WHERE ( DATE_ADD(NOW(), INTERVAL 3 HOUR) > DATE_ADD(DATE_SUB, INTERVAL 30 DAY) AND `active` = 1);';
    this.mysql.query(sql, (error, results) => {
      if (error) {
        console.log(error.sqlMessage);
        return (error.sqlMessage);
      }
      // console.log('UPDATE api_trading, subs users', results);
      return results;
    });
  }

  disableUserExpiredSubsTrial() {
    const sql = 'UPDATE `god_db1`.`api_trading` SET `active_trial` = 1 WHERE ( DATE_ADD(NOW(), INTERVAL 1 HOUR) > DATE_ADD(DATE_TRIAL, INTERVAL 25 HOUR) AND `active_trial` = 0);';
    this.mysql.query(sql, (error, results) => {
      if (error) {
        console.log(error.sqlMessage);
        return (error.sqlMessage);
      }
      // console.log('UPDATE api_trading, trial users', results);
      return results;
    });
  }

  // Смотрим сколько открытых сделок у людей в постегре
  // Если нет открытых сделок, то удлалять из массива фришников
  // на продажу. Им делаем статус 2. Чтобы не попадали ни
  // под условие
  disableUserExpiredSubsForSell() {
    const sql = 'SELECT COUNT(*) as amountorders, userid FROM public.api_trading_users WHERE (typeordbuy = $1 AND typeordsell = $2) GROUP BY userid;';
    const arr = ['BUY', ''];
    const users = [];
    let str = '';
    let res = null;

    this.postgre.query(sql, arr, (err, result) => {
      if (result.rowCount !== 0) {
        result.rows.forEach((dataset) => {
          users.push(dataset.userid);
        });

        if (users.length === result.rowCount) {
          if (this.listApiSubTrialExpiredUsers.length !== 0) {
            const difference = [];
            this.listApiSubTrialExpiredUsers.forEach((dataset) => {
              if (!users.includes(dataset.userid)) {
                difference.push(dataset.userid);
              }
            });

            str = difference.join();
            const sqlMySQL = `UPDATE \`god_db1\`.\`api_trading\` SET \`active_trial\` = 2 WHERE ( userid IN (${str}));`;
            this.mysql.query(sqlMySQL, (error, results) => {
              if (error) {
                console.log(error.sqlMessage);
              }
              res = results;
              // console.log('UPDATE api_trading, disable users for sell', str, results);
            });
          } else {
            // console.log('this.listApiSubTrialExpiredUsers.length is empty');
          }
        }
      } else {
        // console.log('All ok. There are no user for disable sell');
      }
    });

    /*
    this.postgre.query(sql, arr, (err, result) => {
      if (result.rowCount !== 0) {
        result.rows.forEach((dataset) => {
          if (dataset.amountorders === 0) {
            users.push(dataset.userid);
          }
        });

        if (users.length === result.rowCount) {
          str = users.join();
          const sqlMySQL = `UPDATE \`god_db1\`.\`api_trading\` SET \`active_trial\` = 2 WHERE ( userid IN (${str}));`;
          this.mysql.query(sqlMySQL, (error, results) => {
            if (error) {
              console.log(error.sqlMessage);
              // return (error.sqlMessage);
            }
            res = results;
            console.log('UPDATE api_trading, disable users for sell', str, results);
          });
        }
      } else {
        console.log('All ok. There are no user for disable sell');
      }
    });

    if (this.listApiSubTrialExpiredUsers.length !== 0) {
      this.listApiSubTrialExpiredUsers.forEach(async (dataset) => {
        users2.push(dataset.userid);
      });

      const difference = users
        .filter(x => users2.indexOf(x) === -1)
        .concat(users2.filter(x => users.indexOf(x) === -1));

      const str2 = difference.join();
      const sqlMySQL2 = `UPDATE \`god_db1\`.\`api_trading\` SET \`active_trial\` = 2 WHERE ( userid IN (${str2}));`;
      this.mysql.query(sqlMySQL2, (error, results) => {
        if (error) {
          console.log(error.sqlMessage);
          // return (error.sqlMessage);
        }
        console.log('UPDATE api_trading, disable users for sell', str2, results);
        res = results;
      });
    }
    */
    return res;
  }

  sendApiMessageViaTelegramm(symbol, price, action, stoploss, chatid) {
    let msg = `🔔 ${symbol.replace('BTC', '-BTC')}`;
    if (action === 'BUY') {
      msg += `\n📗 ${action} PRICE: ${price}`;
      msg += `\n⛔️ STOPLOSS: ${stoploss.toFixed(8)}`;
    } else {
      msg += `\n📕 ${action} PRICE: ${price}`;
    }

    console.log(this.listApiSubUsers);
    // if (this.listApiSubUsers.length !== 0) {
    //   this.listApiSubUsers.forEach((dataset) => {
    // clientTeleg.sendMessage(dataset.userid, msg, {
    clientTeleg.sendMessage(chatid, msg, {
      disable_web_page_preview: true,
      disable_notification: true,
    });
    //   });
    // }
  }

  static sendBuySellErrorViaTelegramm(str, market, type, userid) {
    const binanceErrorMsgs = [
      'Account has insufficient balance for requested action',
      'Invalid API-key, IP, or permissions for action.',
      'API-key format invalid.',
      'Empty keys',
      'Empty keys or order size',
      'Signature for this request is not valid.',
    ];
    let msg = '';
    let msgToUs = '';

    const obj = JSON.parse(str);
    const codeError = obj.code;
    const msgError = obj.msg;

    msg = `🔔 Error. ${type}. ${market}.`;
    msg += `\n Code: ${codeError}`;
    msg += `\n Message: ${msgError}`;

    msgToUs = `🔔 Error. ${type}. ${market}. User is ${userid}.`;
    msgToUs += `\n Code: ${codeError}`;
    msgToUs += `\n Message: ${msgError}`;

    clientTeleg.sendMessage(142545448, msgToUs, {
      disable_web_page_preview: true,
      disable_notification: true,
    });
    clientTeleg.sendMessage(159856577, msgToUs, {
      disable_web_page_preview: true,
      disable_notification: true,
    });

    if (binanceErrorMsgs.includes(msgError)) {
      clientTeleg.sendMessage(userid, msg, {
        disable_web_page_preview: true,
        disable_notification: true,
      });
    }
  }

  addNewUser(userId) {
    return new Promise((resolve, reject) => {
      const sql = 'INSERT IGNORE INTO `god_db1`.`api_trading` SET ?';
      const activeTrialOrSub = false;
      const datas = {
        userid: userId,
        active: activeTrialOrSub,
        date_trial: new Date(),
        active_trial: activeTrialOrSub,
      };

      this.mysql.query(sql, datas, (error, results) => {
        if (error) {
          // console.log(error.sqlMessage);
          reject(error);
        }

        resolve(results.insertId);
      });
    });
  }

  addNewPayment(userId, stateApi) {
    return new Promise((resolve, reject) => {
      if (stateApi === 0 || stateApi === 2 || stateApi === -1) {
        const sql = 'INSERT IGNORE INTO `god_db1`.`users_payments` SET ?';
        const activeTrialOrSub = false;
        const datas = {
          userid: userId,
          wallet: '',
          invoice_our_id: `${userId}-5`,
          invoce_date: '',
          type_sub: 5,
          payment_code: `${userId}-payment-code-5`,
          invoice_id_other: '',
          price: 5000000, // 0.1 BTC = 10 000 000, 0.05 BTC = 5 000 000, 5000000
          status: activeTrialOrSub,
        };

        this.mysql.query(sql, datas, (error, results) => {
          if (error) {
            // console.log(error.sqlMessage);
            reject(error);
          }

          request({ url: 'http://earntube.info/addr/gen.php' }, () => {
          });
          resolve(results.insertId);
        });
      } else {
        resolve(null);
      }
    });
  }

  addEditFieldMySQL(userId, field, value) {
    return new Promise((resolve, reject) => {
      const sql = `UPDATE \`god_db1\`.\`api_trading\` SET ${field} = ? WHERE (userid = ?)`;
      const datas = [value, userId];

      this.mysql.query(sql, datas, (error) => {
        if (error) {
          // console.log(error.sqlMessage);
          reject(error);
        }

        resolve('update');
      });
    });
  }

  checkExistenceUser(userid) {
    return new Promise((resolve, reject) => {
      this.mysql.query('SELECT * FROM `god_db1`.`api_trading` WHERE (userid = ? AND active = ?)', [userid, 1], (error, results) => {
        if (error) {
          console.log(error.sqlMessage);
          reject(error.sqlMessage);
        }

        resolve(results);
      });
    });
  }

  checkLastPaymentUser(userid) {
    return new Promise((resolve, reject) => {
      this.mysql.query('SELECT `status` FROM `god_db1`.`users_payments` WHERE (userid = ? AND type_sub = ?) ORDER BY id DESC LIMIT 1', [userid, 5], (error, results) => {
        if (error) {
          console.log(error.sqlMessage);
          reject(error.sqlMessage);
        }
        console.log(results);
        if (results.length !== 0) {
          resolve(results[0].status);
        } else {
          resolve(null);
        }
      });
    });
  }

  checkLastApiTrialUserStatus(userid) {
    return new Promise((resolve, reject) => {
      this.mysql.query('SELECT `active_trial` FROM `god_db1`.`api_trading` WHERE (userid = ?) LIMIT 1', [userid], (error, results) => {
        if (error) {
          console.log(error.sqlMessage);
          reject(error.sqlMessage);
        }
        console.log(results);
        if (results.length !== 0) {
          resolve(results[0].active_trial);
        } else {
          resolve(null);
        }
      });
    });
  }

  static isEmptyObj(foo) {
    let result = true;
    Object.keys(foo).forEach((key) => {
      if (Object.prototype.hasOwnProperty.call(foo, key)) {
        result = false;
      }
    });
    return result;
  }
}

module.exports = Apitrading;