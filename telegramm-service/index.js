class Telegrammservice {
  constructor(chatId, connections) {
    this.chatId = chatId;

    this.telegramm = connections.telegrammrobot;
    this.redissubs = connections.redissubs;

    return this;
  }

  teleSendMessage(title, kindOfMenu = {}) {
    // That is Promise object
    return this.telegramm.sendMessage(this.Chatid, title, kindOfMenu, {
      reply_markup: {
        force_reply: true,
      },
    });
  }

  teleSendTextMessage(text) {
    return this.telegramm.sendMessage(this.Chatid, text);
  }

  teleSendTextMessageApi(text) {
    return this.telegramm.sendMessage(this.Chatid, text);
  }

  getAllFieldsFromRedis(userid) {
    return new Promise((resolve, reject) => {
      this.redissubs.redis0.pipeline([
        ['get', `${userid}:api:start`],
        ['get', `${userid}:api:apikey`],
        ['get', `${userid}:api:secretkey`],
        ['get', `${userid}:api:order`],
      ]).exec((err, results) => {
        if (err) reject(err);

        const map = new Map();
        map.set('start', results[0][1]);
        map.set('apikey', results[1][1]);
        map.set('secretkey', results[2][1]);
        map.set('order', results[3][1]);

        resolve(map);
      });
    });
  }

  getOneFieldRedis(userid, field) {
    return new Promise((resolve, reject) => {
      this.redissubs.redis0.pipeline([
        ['get', `${userid}:api:${field}`],
      ]).exec((err, results) => {
        if (err) reject(err);

        const map = new Map();
        map.set(field, results[0][1]);
        resolve(map);
      });
    });
  }

  setOneFieldRedis(userid, setKey, key) {
    return new Promise((resolve, reject) => {
      this.redissubs.redis0.pipeline([
        ['set', `${userid}:api:${setKey}`, key],
      ]).exec((err) => {
        if (err) reject(err);

        resolve(null);
      });
    });
  }

  delAllFieldsRedis(userid) {
    this.redissubs.redis0.pipeline([
      ['del', `${userid}:api:start`],
      ['del', `${userid}:api:apikey`],
      ['del', `${userid}:api:secretkey`],
      ['del', `${userid}:api:order`],
    ]).exec((err) => {
      if (err) console.log(err);
    });
  }

  // static teleSendMessagePayload(lastMsgId, payload) {
  //   return lastMsgId.set(payload.chat.id, payload.message_id);
  // }

  set Chatid(value) {
    this.chatId = value;
  }

  get Chatid() {
    return this.chatId;
  }
}

module.exports = Telegrammservice;