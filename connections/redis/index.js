const Redis = require('ioredis');
const settings = require('./settings-redis');

class Dbredis {
  constructor() {
    this.connection = Dbredis.connectToDb();

    return this.connection;
  }

  static connectToDb() {
    const arr = {};

    // This one is current server redis
    arr.redis0 = new Redis({
      port: settings.port,
      host: process.env.REDIS_HOST0,
      password: process.env.REDIS_PASSWORD0,
      db: process.env.REDIS_DB0,
    });

    // These are other redises, from other servers
    arr.redis1 = new Redis({
      port: settings.port,
      host: process.env.REDIS_HOST1,
      password: process.env.REDIS_PASSWORD1,
      db: process.env.REDIS_DB1,
    });

    arr.redis2 = new Redis({
      port: settings.port,
      host: process.env.REDIS_HOST2,
      password: process.env.REDIS_PASSWORD2,
      db: process.env.REDIS_DB2,
    });

    arr.redis3 = new Redis({
      port: settings.port,
      host: process.env.REDIS_HOST3,
      password: process.env.REDIS_PASSWORD3,
      db: process.env.REDIS_DB3,
    });

    arr.redis4 = new Redis({
      port: settings.port,
      host: process.env.REDIS_HOST4,
      password: process.env.REDIS_PASSWORD4,
      db: process.env.REDIS_DB4,
    });

    arr.redis5 = new Redis({
      port: settings.port,
      host: process.env.REDIS_HOST5,
      password: process.env.REDIS_PASSWORD5,
      db: process.env.REDIS_DB5,
    });

    arr.redis6 = new Redis({
      port: settings.port,
      host: process.env.REDIS_HOST6,
      password: process.env.REDIS_PASSWORD6,
      db: process.env.REDIS_DB6,
    });

    return arr;
  }
}

module.exports = Dbredis;